import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.telagus.com',
  appName: 'telagus',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  },
  plugins: {
    "PushNotifications": {
      "presentationOptions": ["badge", "sound", "alert"]
    },
    "LocalNotifications": {
      "smallIcon": "push_icon",
      "iconColor": "#488AFF",
      "sound": "beep.wav"
    }
  }
};

export default config;
