/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
    "./node_modules/flowbite/**/*.js",
  ],
  theme: {
    extend: {
      colors: {
        'blue#4B4DBC': {
          100: '#EDEEF9',
          200: '#cdcded',
          900: '#4B4DBC'
        },
        'green#68DA44': {
          100: '#F0FCED',
          900: '#68DA44',
        },
        'pink#F5669E': {
          100: '#FEF0F6',
          900: '#F5669E',
        },
        'purple#8257CB': {
          100: '#F3EFFA',
          900: '#8257CB',
        },
        'yellow#FBB722': {
          100: '#FFF8E9',
          900: '#FBB722',
        },
      },
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}

