import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot,RouterStateSnapshot,Router,UrlTree} from '@angular/router';
import { TokenStorageService } from '../service/token-storage.service'; 
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class AuthGuard {
  constructor(public tokenStorage:TokenStorageService, public router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | UrlTree | boolean {
    if (this.tokenStorage.getToken()) {
      return true;
    }else{
      this.router.navigate(['login']);
      return false;
    }
    
  }
}