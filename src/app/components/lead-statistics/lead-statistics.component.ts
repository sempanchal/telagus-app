import { Component, OnInit } from '@angular/core';
import * as moment from "moment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { isEmpty } from 'lodash';
import { map } from 'lodash';

@Component({
  selector: 'app-lead-statistics',
  templateUrl: './lead-statistics.component.html',
  styleUrls: ['./lead-statistics.component.scss'],
})
export class LeadStatisticsComponent  implements OnInit {

  filterOptions:any=[{
    id: 'today', text: 'Today'
  },{
    id: 'last_7_days', text: 'Last 7 Days'
  },{
    id: 'last_15_days', text: 'Last 15 Days'
  },{
    id: 'last_1_month', text: 'Last Month'
  },{
    id: 'last_6_months', text: 'Last 6 Months'
  },{
    id: 'last_year', text: 'Last Year'
  },{
    id: 'lifetime', text: 'Lifetime'
  },{
    id: 'custom', text: 'Custom'
  }];


  constructor(private http: HttpClient,
    private tokenStorage:TokenStorageService) { }

  ngOnInit() {
    let dates=this.getDatesBasesOnSelection('last_7_days');
    this.getCounts(dates);
    this.getQuoteStates(dates);
    this.getOpenTasksCount(dates);
  }

  triggerEvent($event:any){
    if($event.detail.value){ //adding condition as valueChanged emitting twice
      let dates={};
      /*if($event.detail.value =='custom'){
        this.customDateSelected=true;
        this.fromDate={
          year: moment().startOf("months").year(),
          month: moment().startOf("months").month()+1,
          day: moment().startOf("months").date()
        }
        this.toDate={
          year: moment().year(),
          month: moment().month()+1,
          day: moment().date()
        }
        this.formdata.patchValue({
          fromdate:this.fromDate,
          todate:this.toDate
        });

        dates = {'start_date': this.fromDate.year+"-"+this.fromDate.month+"-"+this.fromDate.day, 'end_date': this.toDate.year+"-"+this.toDate.month+"-"+this.toDate.day+" 23:59:59"};
      }else{*/
        //this.customDateSelected=false;
        dates=this.getDatesBasesOnSelection($event.detail.value);
      //}
      //this.select2PreviousValue=e;
      this.getCounts(dates);
      this.getQuoteStates(dates);
      this.getOpenTasksCount(dates);
    }
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  getDatesBasesOnSelection(string:string){
    switch (string){
      case 'today':
        return {'start_date': moment().format('YYYY-MM-DD'), 'end_date': moment().format('YYYY-MM-DD 23:59:59')};
      case 'last_7_days':
        return {'start_date': moment().subtract(7, 'days').format('YYYY-MM-DD'), 'end_date': moment().format('YYYY-MM-DD 23:59:59')};
      case 'last_15_days':
        return {'start_date': moment().subtract(15, 'days').format('YYYY-MM-DD'), 'end_date': moment().format('YYYY-MM-DD 23:59:59')};
      case 'last_1_month':
        return {'start_date': moment().subtract(1, 'month').format('YYYY-MM-DD'), 'end_date': moment().format('YYYY-MM-DD 23:59:59')};
      case 'last_6_months':
        return {'start_date': moment().subtract(6, 'months').format('YYYY-MM-DD'), 'end_date': moment().format('YYYY-MM-DD 23:59:59')};
      case 'last_year':
        return {'start_date': moment().subtract(1, 'year').format('YYYY-MM-DD'), 'end_date': moment().format('YYYY-MM-DD 23:59:59')};
      default :
        return {};
    }
  }

  leads_states:any = 0;
  getCounts(dates:any) {
    this.http.get(
        environment.apiURL + 'all-lead-counts'+(!isEmpty(dates)?'?'+this.toQuery(dates):''),
        this.httpOptions
      ).subscribe((resp:any) => {
        if (resp['leads_count']) {
          this.leads_states = resp['leads_count'];
        } else {
          this.leads_states = 0;
        }
      });
  }

  openTasksCount:any = 0;
  getOpenTasksCount(dates:any) {
    this.http.get(
      environment.apiURL + 'get-open-tasks-count'+(!isEmpty(dates)?'?'+this.toQuery(dates):''),
      this.httpOptions
    ).subscribe((resp:any) => {
      if (resp['count']) {
        this.openTasksCount = resp['count'];
      } else {
        this.openTasksCount = 0;
      }
    });
  }

  estimatesCount:any = 0;
  getQuoteStates(dates:any){
    this.http.get(
      environment.apiURL + 'get-estimates-count'+(!isEmpty(dates)?'?'+this.toQuery(dates):''),
      this.httpOptions
    ).subscribe((resp:any) => {
      if (resp['count']) {
        this.estimatesCount = resp['count'];
      } else {
        this.estimatesCount = 0;
      }
    });
  }

  toQuery(data:any){
    return map(data, (value, key) => `${key}=${value}`)
      .join("&");
  }

}
