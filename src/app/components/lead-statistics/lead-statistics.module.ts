import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadStatisticsComponent } from './lead-statistics.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [LeadStatisticsComponent],
  imports: [
    CommonModule,IonicModule
  ],
  exports: [LeadStatisticsComponent]
})
export class LeadStatisticsModule { }
