import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent  implements OnInit {
  @Input() ajaxcall: boolean | undefined;
  //@Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  user:any = null;
  logoUrl:any = environment.AWSBucketURL+'logo.svg';
  constructor(private tokenStorage:TokenStorageService,
    private router: Router,
    private menuCtrl: MenuController) {

   }

  ngOnInit() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {

      }
      if (event instanceof NavigationEnd) {
        if(this.tokenStorage.getToken()){
          this.user = this.tokenStorage.getUser();
        }
      }
      if (event instanceof NavigationError) {
       
      }
    });

  }

  ajaxCallFunction(ajaxcall:boolean){
    this.ajaxcall = ajaxcall;
    //this.visibleChange.emit(ajaxcall);
  }


  userLogout() {
    this.user = null;
    this.menuCtrl.close();
    this.tokenStorage.signOut();
    this.router.navigateByUrl('login');
  }
}
