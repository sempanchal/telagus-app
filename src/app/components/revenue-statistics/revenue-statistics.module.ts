import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RevenueStatisticsComponent } from './revenue-statistics.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [RevenueStatisticsComponent],
  imports: [
    CommonModule,IonicModule
  ],
  exports:[RevenueStatisticsComponent]
})
export class RevenueStatisticsModule { }
