import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-revenue-statistics',
  templateUrl: './revenue-statistics.component.html',
  styleUrls: ['./revenue-statistics.component.scss'],
})
export class RevenueStatisticsComponent  implements OnInit {
  
    combinedLeadsArray: any;
    currentMonthTotalRevenue: any;
    lastMonthTotalRevenue: any;
    percentageDifference: any;
    httpOptions = {
        headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.tokenStorage.getToken()}`
        })
    }
    constructor(public tokenStorage: TokenStorageService,
        private http: HttpClient) { }

    ngOnInit() {
        this.getRevenueThisMonth();
    }

    getRevenueThisMonth() {
        this.http.get(
        environment.apiURL + 'leads/revenue-this-month',
        this.httpOptions
        ).subscribe((res: any) => {
        if (res) {
            this.combinedLeadsArray = res['combinedLeadsArray'];
            this.currentMonthTotalRevenue = res['currentMonthTotalRevenue'];
            this.lastMonthTotalRevenue = res['lastMonthTotalRevenue'];
            this.percentageDifference = res['percentageDifference'];
        }
        });
    }

}
