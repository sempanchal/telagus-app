import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-conversion-ratio',
  templateUrl: './conversion-ratio.component.html',
  styleUrls: ['./conversion-ratio.component.scss'],
})
export class ConversionRatioComponent  implements OnInit {
  userLeadData: any;
  constructor(public tokenStorage: TokenStorageService,
    private http: HttpClient) { }
  
  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  ngOnInit() {
    let datesData = { 'revenue_type': 'lead_source_type', 'start_date': moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD'), 'end_date': moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD 23:59:59') };
    this.conversionRatio(datesData);
  }


  conversionRatio(dates: any) {
   

    this.http.post(
      environment.apiURL + 'leads/conversion-ratio',dates,
      this.httpOptions
      ).subscribe((res: any) => {
        if (res) {
          this.userLeadData = res.userLeadData;
        }
      });

      /*return this.http.post(environment.apiURL + 'leads/conversion-ratio', data, this.httpOptions).pipe(
        catchError(this.errorHandler)
      )*/

    
  }

}
