import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConversionRatioComponent } from './conversion-ratio.component';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [ConversionRatioComponent],
  imports: [
    CommonModule,IonicModule
  ],
  exports:[ConversionRatioComponent]
})
export class ConversionRatioModule { }
