import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeCardComponent } from './time-card.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [TimeCardComponent],
  imports: [CommonModule,IonicModule],
  exports: [TimeCardComponent]
})
export class TimeCardModule { }
