import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as moment from "moment";
import * as moment1 from 'moment-timezone';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-time-card',
  templateUrl: './time-card.component.html',
  styleUrls: ['./time-card.component.scss'],
})
export class TimeCardComponent  implements OnInit {

  isLogin:any = false;
  loginTime:any;
  memberInOffice:any = 0;
  memberNotInOffice:any = 0;
  isClicked:boolean = false;
  isConnected:any = false;
  members_in_office_list:any = [];
  members_not_in_office_list:any = [];
  members_in_list:any = [];
  time_completed:any = null;

  constructor(
    public tokenStorage: TokenStorageService,
    private http: HttpClient,
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  ngOnInit() {
    this.attendanceInfo();
    this.inTime();
  }

  attendanceInfo(){
    let apiurl = environment.apiURL + 'attendance/get-attendance-info';
    return this.http.get(apiurl, this.httpOptions).subscribe({
      next:(res:any) => {
          res.members_in_list.forEach((member:any) => {
            this.members_in_office_list.push(member);
        });
  
        for(let key in  res.members_not_in_office_list) {
          this.members_not_in_office_list.push(res.members_not_in_office_list[key]);
        }
  
        this.isLogin = res.is_logged_in;
        this.loginTime = res.in_time;
        this.memberInOffice=res.members_in_office;
        this.memberNotInOffice=res.members_not_in_office;
        this.isConnected = true;
        this.time_completed = res.time_completed;
      
      
      },
    });
  }

  inTime(){
    var current = moment1(new Date()).tz(this.tokenStorage.getTimeZone());
  }

  dateToFromNowDaily( myDate:any ) {
    // get from-now for this date
    var fromNow = moment( myDate ).format("D MMM YYYY hh:mm A");
    var time = moment( myDate ).format("hh:mm A");

    // ensure the date is displayed with today and yesterday
    return moment( myDate ).calendar( null, {
      // when the date is closer, specify custom values
      lastDay:  '[Yesterday at '+time+']',
      sameDay:  '[Today at '+time+']',
      // when the date is further away, use from-now functionality
      sameElse: function () {
        return '['+fromNow+']';
      }
    });
  }

  stratStopWork(){
    this.isClicked = true;
    this.http.post( environment.apiURL + 'attendance/start-stop-work',{},this.httpOptions).subscribe({
      next:(res:any) => {
        if(res.is_error){
          Swal.fire(res.msg, '','error');
          this.isClicked = false;
        }else{
          this.isLogin = res.logged_in;
          this.isClicked = false;
          if(res.logged_in_time){
            this.loginTime = res.logged_in_time;
          }
          this.memberInOffice=res.members_in_office;
          this.memberNotInOffice=res.members_not_in_office;
        }
      },
    });
  }
}
