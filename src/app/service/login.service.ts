import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders ,HttpErrorResponse} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  errorHandler(error:any) {
    let errorMessage = '';
    if(error instanceof HttpErrorResponse) {
      errorMessage = error.error;
    } else {
      errorMessage = error;
    }
    return throwError(errorMessage);
  }


  login(user:any){
    return this.httpClient.post( environment.apiURL +'login', user).pipe(
      catchError(this.errorHandler)
    )
  }
}
