import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const USER_INFO_KEY = 'auth-user-info';
const XERO_INFO_KEY = 'xero-info';
const RETURN_URL_KEY = 'return-url';
const DEVICE_KEY = 'device-key';
const CHOOSE_MODULE_INFO_KEY = 'choose-module-info';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  
  constructor() { }

  public saveToken(token: string): void {
    localStorage.removeItem(TOKEN_KEY);
    let now = new Date();
    let item: any;

    // `item` is an object which contains the original value
    // as well as the time when it's supposed to expire
    item = {
      value: token,
      expiry: now.getTime() + (60000 * 60),
    }

    localStorage.setItem(TOKEN_KEY, JSON.stringify(item));

    const itemStr = localStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    localStorage.removeItem(USER_KEY);
    localStorage.setItem(USER_KEY, JSON.stringify(user));
  }
  public getUser(): any {
    let userData = {user:'',userInfo:''};
    const user = localStorage.getItem(USER_KEY);
    if (user) {
      userData.user = JSON.parse(user);
    }
    const userInfo = localStorage.getItem(USER_INFO_KEY);
    if (userInfo) {
      userData.userInfo = JSON.parse(userInfo);
    }
    return userData;
  }

  public saveUserInfo(userInfo: any): void {
    localStorage.removeItem(USER_INFO_KEY);
    localStorage.setItem(USER_INFO_KEY, JSON.stringify(userInfo));
  }

  public saveXeroInfo(xeroInfo: any): void {
    localStorage.removeItem(XERO_INFO_KEY);
    localStorage.setItem(XERO_INFO_KEY, JSON.stringify(xeroInfo));
  }

  public saveChooseModuleInfo(chooseModuleInfo: any): void {
    localStorage.removeItem(CHOOSE_MODULE_INFO_KEY);
    localStorage.setItem(CHOOSE_MODULE_INFO_KEY, JSON.stringify(chooseModuleInfo));
  }

  public getToken(): string | null {
    const itemStr = localStorage.getItem(TOKEN_KEY);

    // if the item doesn't exist, return null
    if (!itemStr) {
      return null
    }

    try {
      const item = JSON.parse(itemStr);
      const now = new Date()
      // compare the expiry time of the item with the current time
      if (now.getTime() > item.expiry) {
        // If the item is expired, delete the item from storage
        localStorage.removeItem(TOKEN_KEY);
        localStorage.clear();
        return null;
      } else {
        this.saveToken(item.value);
      }
      return item.value
    } catch (e) {
      localStorage.removeItem(TOKEN_KEY);
      localStorage.clear();
      return null;
    }
  }

  public formatPrice(val: any, onlySymbol: any = false) {
    let clientSetting = this.getUser().user.client.client_settings;
    let currency = clientSetting.currency;
    let currency_symbol = clientSetting.currency_symbol;
    let currency_position = clientSetting.currency_position;
    if (onlySymbol) {
      return currency_symbol;
    }
    if (currency_position == 'left') {
      return currency_symbol + '' + val;
    } else {
      return val + '' + currency_symbol;
    }
  }

  signOut(): void {
    let username:any = (localStorage.getItem('telagus_username'))?localStorage.getItem('telagus_username'):'';
    let password:any = (localStorage.getItem('telagus_password'))?localStorage.getItem('telagus_password'):'';
    let remember_me:any = (localStorage.getItem('remember_me'))?localStorage.getItem('remember_me'):false;
    localStorage.clear();
    if(remember_me){
      localStorage.setItem('telagus_username', username);
      localStorage.setItem('telagus_password', password);
      localStorage.setItem('remember_me', remember_me);
    }
    document.body.classList.remove('togglemenusidebarActivebody');
  }

  public getTimeZone(){
    if(this.getUser().timezone){
      return this.getUser().timezone;
    }else if (this.getUser().client?.timezone){
      return this.getUser().client?.timezone
    }else {
      return 'Europe/London';
    }
  }

  public getLeadPositionNameFromId(id:any) {
    let lead_positions = this.getUser().client.lead_position;
    for (let lead_position of lead_positions) {
      if (lead_position.id == id) {
        return lead_position.name;
      }
    }
    return "Lead position with id " + id;
  }

  public getUserInfo(): any {
    const userInfo = localStorage.getItem(USER_INFO_KEY);
    if (userInfo) {
      return JSON.parse(userInfo);
    }
    return {};
  }

  public getTeamUserNameFromId(id:number) {
    let users = this.getUser().client.users;
    for (let user of users) {
      if (user.id == id) {
        let name = (user.first_name) ? user.first_name + " " : "";
        name += (user.last_name) ? user.last_name : "";
        if (name == '') {
          name = "User with id " + id;
        }
        return name;
      }
    }
    return "User with id " + id;
  }

}
