import { Injectable } from '@angular/core';
import { LocalNotifications } from '@capacitor/local-notifications';
import { FileOpener } from '@capawesome-team/capacitor-file-opener';
import * as moment from 'moment';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
    private tokenStorage:TokenStorageService,
  ) { }

  dateToFromNowDaily( myDate:any ) {
    // get from-now for this date
    var fromNow = moment( myDate ).format("D MMM YYYY hh:mm A");
    var time = moment( myDate ).format("hh:mm A");

    // ensure the date is displayed with today and yesterday
    return moment( myDate ).calendar( null, {
      // when the date is closer, specify custom values
      lastDay:  '[Yesterday at '+time+']',
      sameDay:  '[Today at '+time+']',
      // when the date is further away, use from-now functionality
      sameElse: function () {
        return '['+fromNow+']';
      }
    });
  }
  
  dateFrom( myDate:any ) {
    // get from-now for this date
    return  moment( myDate ).format("D MMM YYYY");
  }

  addLeadingZeros(num: number, totalLength: number): string {
    return String(num).padStart(totalLength, '0');
  }

  formatPrice(val: any) {
    //console.log(this.tokenStorage.getUser());
    let clientSetting = this.tokenStorage.getUser().user.client.client_settings;
    let currency = clientSetting.currency;
    let currency_symbol = clientSetting.currency_symbol;
    let currency_position = clientSetting.currency_position;
    if (currency_symbol == null) {
      currency_symbol = ''
    }
    if (currency_position == 'left') {
      return currency_symbol + '' + val;
    } else {
      return val + '' + currency_symbol;
    }
  }

  blobToFile(data: any, type: string, fileName: string) {
    const a = document.createElement('a');
    document.body.appendChild(a);
    a.style.display = 'none';
    const blob = new Blob([data], {type: type});
    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
    
  }

  blobToSaveAs(fileName: string, data: any, type: string) {
    try {
      const blob = new Blob([data], {type: type});
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      if (link.download !== undefined) { // feature detection
        link.setAttribute('href', url);
        link.setAttribute('download', fileName);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    } catch (e) {
      console.error('BlobToSaveAs error', e);
    }
  }

  async showLocalNotification(title:any,body:any = '') {
    await LocalNotifications.schedule({
        notifications: [
            {
              title: title,
              body: body,
              id: Math.ceil(Math.random() * 100), // any random int
              //schedule: { at: new Date(Date.now()) },
              ongoing: false, // (optional, default: false)
              smallIcon: 'res://drawable/push_icon',
              largeIcon: 'res://drawable/push_icon',
              
              //if ongoing:true, this notification can't be cleared
            }
        ]
    });
  }

  public async openFileWithType(filePath: string) {
    await FileOpener.openFile({
      path: filePath,
    });
  };

  get_url_extension(url: any) {
    let defaultImage = ''
    let getext = url.split(/[#?]/)[0].split('.').pop().trim();
    var imageExt = ['jpeg', 'gif', 'png', 'apng', 'svg', 'bmp', 'ico', 'png', 'jpg'];
    if (imageExt.includes(getext)) {
      defaultImage = environment.AWSBucketURL + 'estimate-attachment/' + url;
    } else if (getext === 'pdf') {
      defaultImage = 'https://img.icons8.com/fluency/100/000000/pdf.png';
    } else if (getext === 'docx') {
      defaultImage = 'https://img.icons8.com/color/100/000000/microsoft-word-2019--v2.png';
    } else if (getext === 'xlsx') {
      defaultImage = 'https://img.icons8.com/color/100/000000/microsoft-excel-2019--v1.png';
    } else {
      defaultImage = 'https://img.icons8.com/fluency/100/000000/file.png';
    }
    return defaultImage;
  }

}
