import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from './token-storage.service';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class QuotesService {

  constructor(private http: HttpClient,
    private tokenStorage: TokenStorageService
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error instanceof HttpErrorResponse) {
      errorMessage = error.error;
    } else {
      errorMessage = error;
    }
    return throwError(errorMessage);
  }

 quoteListing(data: any){
    let apiurl = environment.apiURL + 'estimate-app-list';
    return this.http.post(apiurl,data ,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  itemListing(){
    let apiurl = environment.apiURL + 'items/get-client-items-data';
    return this.http.get(apiurl,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  } 
  
  quoteWithItems(id:number){
    let apiurl = environment.apiURL + 'quote-with-items/'+id;
    return this.http.get(apiurl,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

  downloadPDF(id:any){
    let url = environment.apiURL+"estimate-pdf/"+id;
    return this.http.get(url,{
      responseType: 'arraybuffer', headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    })
    .pipe(
      catchError(this.errorHandler)
    )
  }

  // quoteMail(data: any, id:Number){
  //   let apiurl = environment.apiURL + 'new-estimate-mail/'+id;
  //   return this.http.post(apiurl,data ,{
  //     headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
  //   }).pipe(catchError(this.errorHandler));
  // }

  sendQuoteEmail(data:any,id:number){
    var formData = new FormData();
    formData.append("to", data.to);
    if(data.msg){
      formData.append("msg", data.msg);
    }
    for (var i = 0; i < data.cc.length; i++) {
      formData.append("cc[]", data.cc[i]);
    }
    for (var i = 0; i < data.bcc.length; i++) {
      formData.append("bcc[]", data.bcc[i]);
    }
    for (var i = 0; i < data.reply_to.length; i++) {
      formData.append("replied_to_email_list[]", data.reply_to[i]);
    }
    formData.append("replied_to_email_list[]",data.default_reply_to);
    let apiURL = environment.apiURL + 'new-estimate-mail/'+id;
    return this.http.post(apiURL, formData, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    })
  }

  addFilestoinQuotes(data: any) {
    var formData = new FormData();
    formData.append("file[]", data.file);
    formData.append("title", data.title);
    let apiURL = environment.apiURL + 'estimate-attachment';
    return this.http.post(apiURL, formData, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    })
  }
  
  estimateAttachment() {
    let apiURL = environment.apiURL + 'get-estimate/attachment';
    return this.http.get(apiURL, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    });
  }

  setAttachmentsToQuote(param:any){
    let apiURL = environment.apiURL + 'estimate-attachment-list';
    return this.http.post(apiURL, param, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    })
  }

  getAttachmentForIndQuote(id:any){
    let apiURL = environment.apiURL + 'estimate-attachment-list/estimate/'+id;
    return this.http.get(apiURL, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    })
  }

  deleteAttachmentForIndQuote(id:any){
    let url = environment.apiURL+"estimate-attachment-list/"+id;
    return this.http.delete(url,this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  addDiscount(data: any) {
    let url = environment.apiURL+"estimates/add-discount";
    return this.http.post(url, data, this.httpOptions).pipe(
      catchError(this.errorHandler)
    )
  }

  removeDiscount(quote_id: any) {
    let url = environment.apiURL + "estimates/remove-discount?quote_id="+quote_id;
    return this.http.delete(url,this.httpOptions).pipe(
      catchError(this.errorHandler)
    )
  }

}
