import { Injectable } from '@angular/core';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class LeadFilterService {

  constructor(
    public tokenStorage: TokenStorageService,
    private http: HttpClient
    ) { }

  getLeadFilters(){
    // let leadSourceData = this.tokenStorage.getUser()?.lead_source_data;
    // let leadSourceArray:any = [];
    // leadSourceData?.forEach(element => {
    //   leadSourceArray.push(element.lead_source);
    // });
    return [{
      filter_group:{
        name: "Contact Information",
        filters:[
          {"filter_name": "lead_contacts.first_name", "filter_title": "First Name", "filter_type":"input"},
          {"filter_name": "lead_contacts.last_name", "filter_title": "Last Name", "filter_type":"input"},
          {"filter_name": "lead_contacts.email", "filter_title": "Email", "filter_type":"input"},
          {"filter_name": "lead_contacts.mobile_number", "filter_title": "Mobile Number", "filter_type":"input"},
          {"filter_name": "lead_contacts.is_primary_contact", "filter_title": "Primary Contact", "filter_type":"boolean"},
          {"filter_name": "lead_contacts.gender", "filter_title": "Gender", "options":['male','female'], "filter_type":'dropdown'},
          {"filter_name": "lead_contacts.job_title", "filter_title": "Job Title", "filter_type":"input"},
          {"filter_name": "lead_contacts.unsubscribed", "filter_title": "Unsubscribed", "filter_type":"boolean"},
          {"filter_name": "lead_created_at", "filter_title": "Lead Created At", "filter_type":"date"},
        ]
      }
    },{
      filter_group:{
        name: "Company Information",
        filters:[
          {"filter_name": "lead_companies.company_name", "filter_title": "Company Name", "filter_type":"input"},
          {"filter_name": "lead_companies.is_primary_company", "filter_title": "Primary Company", "filter_type":"boolean"},
          {"filter_name": "lead_companies.company_service", "filter_title": "Company Service", "filter_type":"input"},
          {"filter_name": "lead_companies.phone_number", "filter_title": "Company Phone Number", "filter_type":"input"},
          {"filter_name": "lead_companies.street_address", "filter_title": "Street Address", "filter_type":'input'},
          {"filter_name": "lead_companies.street_address2", "filter_title": "Street Address 2", "filter_type":'input'},
          {"filter_name": "lead_companies.locality", "filter_title": "Locality", "filter_type":'input'},
          {"filter_name": "lead_companies.city", "filter_title": "City", "filter_type":'input'},
          {"filter_name": "lead_companies.state", "filter_title": "State", "filter_type":'input'},
          {"filter_name": "lead_companies.postcode", "filter_title": "Postcode", "filter_type":'input'},
          {"filter_name": "lead_companies.year_founded", "filter_title": "Year Founded", "filter_type":"year"},
          {"filter_name": "company_created_at", "filter_title": "Company Created At", "filter_type":"date"},
          {"filter_name": "company_updated_at", "filter_title": "Company Updated At", "filter_type":"date"},
        ]
      }
    },{
      filter_group:{
        name: "Lead Information",
        filters:[
          {"filter_name": "leads.assigned_to", "filter_title": "Assigned To", "filter_type":"dropdown"},
          // {"filter_name": "leads.lead_source", "filter_title": "Lead Source", "options":leadSourceArray, "filter_type":'dropdown'},
          {"filter_name": "leads.form", "filter_title": "Form", "filter_type":"input"},
          {"filter_name": "leads.form_page", "filter_title": "Form Page", "filter_type":"input"},
          //{"filter_name": "leads.price", "filter_title": "Price", "filter_type":"slider"},
          {"filter_name": "leads.lead_position_id", "filter_title": "Lead Position", "filter_type":"dropdown"},
          {"filter_name": "leads.lead_position_stage_id", "filter_title": "Lead Position Stage", "filter_type":"dropdown"}
        ]
      }
    },{
      filter_group:{
        name: "Custom Fields",
        filters:this.getAllCustomFieldsofClient()
      }
    }
    ];
  }

  custom_fields_filtration : any = [];
  getAllCustomFieldsofClient(){
    let current_user = this.tokenStorage.getUser().user.client_id;
    this.custom_fields_filtration = [];
    this.http.get(environment.apiURL +"custom-fields/client-id/"+current_user,{
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).subscribe((res:any)=>{
      res.custom_fields_data.forEach((custom_field:any) => {
        this.custom_fields_filtration.push({"filter_name": "custom_field_values_"+custom_field.id+".value", "filter_title": custom_field.title, "filter_type":"input"});
      });
    })
    return this.custom_fields_filtration;
  }

  getDateOptions() {
    return [{
      id: "today",
      text: "Today"
    },{
      id: "yesterday",
      text: "Yesterday"
    },{
      id: "tomorrow",
      text: "Tomorrow"
    },{
      id: "this_week",
      text: "This week"
    },{
      id: "this_week_so_far",
      text: "This week so far"
    },{
      id: "last_week",
      text: "Last week"
    },{
      id: "next_week",
      text: "Next week"
    },{
      id: "this_month",
      text: "This month"
    },{
      id: "this_month_so_far",
      text: "This month so far"
    },{
      id: "last_month",
      text: "Last month"
    },{
      id: "next_month",
      text: "Next month"
    },{
      id: "this_quarter",
      text: "This quarter"
    },{
      id: "this_quarter_so_far",
      text: "This quarter so far"
    },{
      id: "this_fiscal_quarter",
      text: "This fiscal quarter"
    },{
      id: "this_fiscal_quarter_so_far",
      text: "This fiscal quarter so far"
    },{
      id: "last_quarter",
      text: "Last quarter"
    },{
      id: "last_fiscal_quarter",
      text: "Last fiscal quarter"
    },{
      id: "next_quarter",
      text: "Next quarter"
    },{
      id: "next_fiscal_quarter",
      text: "Next fiscal quarter"
    },{
      id: "this_year",
      text: "This year"
    },{
      id: "this_year_so_far",
      text: "This year so far"
    },{
      id: "this_fiscal_year",
      text: "This fiscal year"
    },{
      id: "this_fiscal_year_so_far",
      text: "This fiscal year so far"
    },{
      id: "last_year",
      text: "Last year"
    },{
      id: "last_fiscal_year",
      text: "Last fiscal year"
    },{
      id: "next_year",
      text: "Next year"
    },{
      id: "next_fiscal_year",
      text: "Next fiscal year"
    },{
      id: "last_7_days",
      text: "Last 7 days"
    },{
      id: "last_14_days",
      text: "Last 14 days"
    },{
      id: "last_30_days",
      text: "Last 30 days"
    },{
      id: "last_60_days",
      text: "Last 60 days"
    },{
      id: "last_90_days",
      text: "Last 90 days"
    },{
      id: "last_365_days",
      text: "Last 365 days"
    }];
  }

  getDateValueFromStringValue(dateString: string) {
    switch (dateString) {
      case "today":
        return moment().format("YYYY-MM-D");
      case "yesterday":
        return moment().subtract(1, "days").format("YYYY-MM-D");
      case "tomorrow":
        return moment().add(1, "days").format("YYYY-MM-D");
      case "this_week":
        return [moment().startOf('isoWeek').format('YYYY-MM-D HH:mm:ss'), moment().endOf('isoWeek').format('YYYY-MM-D HH:mm:ss')]
      case "this_week_so_far":
        return [moment().startOf('isoWeek').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')]
      case "last_week":
        return [moment().subtract(7, "days").startOf('isoWeek').format('YYYY-MM-D HH:mm:ss'), moment().subtract(7, "days").endOf('isoWeek').format('YYYY-MM-D HH:mm:ss')]
      case "next_week":
        return [moment().add(7, "days").startOf('isoWeek').format('YYYY-MM-D HH:mm:ss'), moment().add(7, "days").endOf('isoWeek').format('YYYY-MM-D HH:mm:ss')]
      case "this_month":
        return [moment().startOf('month').format('YYYY-MM-D HH:mm:ss'), moment().endOf('month').format('YYYY-MM-D HH:mm:ss')]
      case "this_month_so_far":
        return [moment().startOf('month').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')]
      case "last_month":
        return [moment().subtract('1', 'month').startOf('month').format('YYYY-MM-D HH:mm:ss'), moment().subtract('1', 'month').endOf('month').format('YYYY-MM-D HH:mm:ss')]
      case "next_month":
        return [moment().add('1', 'month').startOf('month').format('YYYY-MM-D HH:mm:ss'), moment().add('1', 'month').endOf('month').format('YYYY-MM-D HH:mm:ss')]
      case "this_quarter":
        return [moment().startOf('quarter').format('YYYY-MM-D HH:mm:ss'), moment().endOf('quarter').format('YYYY-MM-D HH:mm:ss')];
      case "this_quarter_so_far":
        return [moment().startOf('quarter').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "this_fiscal_quarter":
        return this.getFiscalQuarter();
      case "this_fiscal_quarter_so_far":
        return this.getFiscalQuarterSoFar();
      case "last_quarter":
        return [moment().subtract('3', 'month').startOf('quarter').format('YYYY-MM-D HH:mm:ss'), moment().subtract('3', 'month').endOf('quarter').format('YYYY-MM-D HH:mm:ss')];
      case "last_fiscal_quarter":
        return this.getLastFiscalQuarter();
      case "next_quarter":
        return [moment().add('3', 'month').startOf('quarter').format('YYYY-MM-D HH:mm:ss'), moment().add('3', 'month').endOf('quarter').format('YYYY-MM-D HH:mm:ss')];
      case "next_fiscal_quarter":
        return this.getNextFiscalQuarter();
      case "this_year":
        return [moment().startOf('year').format('YYYY-MM-D HH:mm:ss'), moment().endOf('year').format('YYYY-MM-D HH:mm:ss')];
      case "this_fiscal_year":
        return this.getCurrentFiscalYear();
      case "this_year_so_far":
        return [moment().startOf('year').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "this_fiscal_year_so_far":
        return this.getCurrentFiscalYearSoFar();
      case "last_year":
        return [moment().subtract('1', 'year').startOf('year').format('YYYY-MM-D HH:mm:ss'), moment().subtract('1', 'year').endOf('year').format('YYYY-MM-D HH:mm:ss')];
      case "last_fiscal_year":
        return this.getLastFiscalYear();
      case "next_year":
        return [moment().add('1', 'year').startOf('year').format('YYYY-MM-D HH:mm:ss'), moment().add('1', 'year').endOf('year').format('YYYY-MM-D HH:mm:ss')];
      case "next_fiscal_year":
        return this.getNextFiscalYear();
      case "last_7_days":
        return [moment().subtract('7', 'day').startOf('day').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "last_14_days":
        return [moment().subtract('14', 'day').startOf('day').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "last_30_days":
        return [moment().subtract('30', 'day').startOf('day').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "last_60_days":
        return [moment().subtract('60', 'day').startOf('day').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "last_90_days":
        return [moment().subtract('90', 'day').startOf('day').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      case "last_365_days":
        return [moment().subtract('365', 'day').startOf('day').format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
      default:
        return [];
    }
  }

  getFiscalQuarter(){
    let obj:any = {};
    if(parseInt(moment().format('M'))<4 || (parseInt(moment().format('M'))==4 && moment().date()<6)){
      obj.quarter1 = {start:moment().subtract('1','year').month(3).startOf('month').add('5','days'),end:moment().subtract('1','year').month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().subtract('1','year').month(6).startOf('month').add('5','days'),end:moment().subtract('1','year').month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().subtract('1','year').month(9).startOf('month').add('5','days'),end:moment().subtract('1','year').month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().subtract('1','year').month(0).startOf('month').add('5','days').add('years',1),end:moment().subtract('1','year').month(2).endOf('month').add('5','days').add(1,'years')};
    }else{
      obj.quarter1 = {start:moment().month(3).startOf('month').add('5','days'),end:moment().month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().month(6).startOf('month').add('5','days'),end:moment().month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().month(9).startOf('month').add('5','days'),end:moment().month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().month(0).startOf('month').add('5','days').add('years',1),end:moment().month(2).endOf('month').add('5','days').add(1,'years')};
    }
    if(moment().isBetween(obj.quarter1.start, obj.quarter1.end)){
      return [obj.quarter1.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter1.end.format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().isBetween(obj.quarter2.start, obj.quarter2.end)){
      return [obj.quarter2.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter2.end.format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().isBetween(obj.quarter3.start, obj.quarter3.end)){
      return [obj.quarter3.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter3.end.format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [obj.quarter4.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter4.end.format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getFiscalQuarterSoFar(){
    let obj:any = {};
    if(parseInt(moment().format('M'))<4 || (parseInt(moment().format('M'))==4 && moment().date()<6)){
      obj.quarter1 = {start:moment().subtract('1','year').month(3).startOf('month').add('5','days'),end:moment().subtract('1','year').month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().subtract('1','year').month(6).startOf('month').add('5','days'),end:moment().subtract('1','year').month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().subtract('1','year').month(9).startOf('month').add('5','days'),end:moment().subtract('1','year').month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().subtract('1','year').month(0).startOf('month').add('5','days').add('years',1),end:moment().subtract('1','year').month(2).endOf('month').add('5','days').add(1,'years')};
    }else{
      obj.quarter1 = {start:moment().month(3).startOf('month').add('5','days'),end:moment().month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().month(6).startOf('month').add('5','days'),end:moment().month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().month(9).startOf('month').add('5','days'),end:moment().month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().month(0).startOf('month').add('5','days').add('years',1),end:moment().month(2).endOf('month').add('5','days').add(1,'years')};
    }
    if(moment().isBetween(obj.quarter1.start, obj.quarter1.end)){
      return [obj.quarter1.start.format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().isBetween(obj.quarter2.start, obj.quarter2.end)){
      return [obj.quarter2.start.format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().isBetween(obj.quarter3.start, obj.quarter3.end)){
      return [obj.quarter3.start.format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [obj.quarter4.start.format('YYYY-MM-D HH:mm:ss'), moment().format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getLastFiscalQuarter(){
    let obj:any = {};
    if(parseInt(moment().subtract('3','month').format('M'))<4 || (parseInt(moment().subtract('3','month').format('M'))==4 && moment().date()<6)){
      obj.quarter1 = {start:moment().subtract('1','year').subtract('3','month').month(3).startOf('month').add('5','days'),end:moment().subtract('1','year').subtract('3','month').month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().subtract('1','year').subtract('3','month').month(6).startOf('month').add('5','days'),end:moment().subtract('1','year').subtract('3','month').month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().subtract('1','year').subtract('3','month').month(9).startOf('month').add('5','days'),end:moment().subtract('1','year').subtract('3','month').month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().subtract('1','year').subtract('3','month').month(0).startOf('month').add('5','days').add(1,'years'),end:moment().subtract('1','year').subtract('3','month').month(2).endOf('month').add('5','days').add(1,'years')};
    }else{
      obj.quarter1 = {start:moment().subtract('3','month').month(3).startOf('month').add('5','days'),end:moment().subtract('3','month').month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().subtract('3','month').month(6).startOf('month').add('5','days'),end:moment().subtract('3','month').month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().subtract('3','month').month(9).startOf('month').add('5','days'),end:moment().subtract('3','month').month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().subtract('3','month').month(0).startOf('month').add('5','days').add('years',1),end:moment().subtract('3','month').month(2).endOf('month').add('5','days').add(1,'years')};
    }
    if(moment().subtract('3','month').isBetween(obj.quarter1.start, obj.quarter1.end)){
      return [obj.quarter1.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter1.end.format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().subtract('3','month').isBetween(obj.quarter2.start, obj.quarter2.end)){
      return [obj.quarter2.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter2.end.format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().subtract('3','month').isBetween(obj.quarter3.start, obj.quarter3.end)){
      return [obj.quarter3.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter3.end.format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [obj.quarter4.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter4.end.format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getNextFiscalQuarter(){
    let obj:any = {};
    if(parseInt(moment().add('3','month').format('M'))<4 || (parseInt(moment().add('3','month').format('M'))==4 && moment().date()<6)){
      obj.quarter1 = {start:moment().subtract('1','year').add('3','month').month(3).startOf('month').add('5','days'),end:moment().subtract('1','year').add('3','month').month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().subtract('1','year').add('3','month').month(6).startOf('month').add('5','days'),end:moment().subtract('1','year').add('3','month').month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().subtract('1','year').add('3','month').month(9).startOf('month').add('5','days'),end:moment().subtract('1','year').add('3','month').month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().subtract('1','year').add('3','month').month(0).startOf('month').add('5','days').add(1,'year'),end:moment().subtract('3','month').month(2).endOf('month').add('5','days').add(1,'years')};
    }else{
      obj.quarter1 = {start:moment().add('3','month').month(3).startOf('month').add('5','days'),end:moment().add('3','month').month(5).endOf('month').add('5','days')};
      obj.quarter2 = {start:moment().add('3','month').month(6).startOf('month').add('5','days'),end:moment().add('3','month').month(8).endOf('month').add('5','days')};
      obj.quarter3 = {start:moment().add('3','month').month(9).startOf('month').add('5','days'),end:moment().add('3','month').month(11).endOf('month').add('5','days')};
      obj.quarter4 = {start:moment().add('3','month').month(0).startOf('month').add('5','days').add(1,"year"),end:moment().add('3','month').month(2).endOf('month').add('5','days').add(1,'years')};
    }
    if(moment().add('3','month').isBetween(obj.quarter1.start, obj.quarter1.end)){
      return [obj.quarter1.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter1.end.format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().add('3','month').isBetween(obj.quarter2.start, obj.quarter2.end)){
      return [obj.quarter2.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter2.end.format('YYYY-MM-D HH:mm:ss')];
    }else if(moment().add('3','month').isBetween(obj.quarter3.start, obj.quarter3.end)){
      return [obj.quarter3.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter3.end.format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [obj.quarter4.start.format('YYYY-MM-D HH:mm:ss'), obj.quarter4.end.format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getCurrentFiscalYear(){
    if(parseInt(moment().format('M'))<4 || (parseInt(moment().format('M'))==4 && moment().date()<6)){
      return [moment().subtract('1','year').month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().subtract('1','year').month(2).endOf('month').add('5','days').add(1,'years').format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [moment().month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().month(2).endOf('month').add('5','days').add(1,'years').format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getCurrentFiscalYearSoFar(){
    if(parseInt(moment().format('M'))<4 || (parseInt(moment().format('M'))==4 && moment().date()<6)){
      return [moment().subtract('1','year').month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [moment().month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getLastFiscalYear(){
    if(parseInt(moment().subtract('1','year').format('M'))<4 || (parseInt(moment().subtract('1','year').format('M'))==4 && moment().subtract('1','year').date()<6)){
      return [moment().subtract('2','years').month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().subtract('2','years').month(2).endOf('month').add('5','days').add(1,'years').format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [moment().subtract('1','year').month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().subtract('1','year').month(2).endOf('month').add('5','days').add(1,'years').format('YYYY-MM-D HH:mm:ss')];
    }
  }

  getNextFiscalYear(){
    if(parseInt(moment().add('1','year').format('M'))<4 || (parseInt(moment().add('1','year').format('M'))==4 && moment().add('1','year').date()<6)){
      return [moment().month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().month(2).endOf('month').add('5','days').add(1,'years').format('YYYY-MM-D HH:mm:ss')];
    }else{
      return [moment().add('1','year').month(3).startOf('month').add('5','days').format('YYYY-MM-D HH:mm:ss'),moment().add('1','year').month(2).endOf('month').add('5','days').add(1,'years').format('YYYY-MM-D HH:mm:ss')];
    }
  }
}
