import { TestBed } from '@angular/core/testing';

import { LeadFilterService } from './lead-filter.service';

describe('LeadFilterService', () => {
  let service: LeadFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LeadFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
