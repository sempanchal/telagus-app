import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from './token-storage.service';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LeadsService {

  constructor(
    private http: HttpClient,
    private tokenStorage: TokenStorageService
  ) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error instanceof HttpErrorResponse) {
      errorMessage = error.error;
    } else {
      errorMessage = error;
    }
    return throwError(errorMessage);
  }

  getFieldGridOfLeads(user_id: string){
    let apiurl = environment.apiURL + 'get-field-grid-of-leads/' + user_id;
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

  getLeadsDataFun(id: any){
    let apiurl = environment.apiURL + 'lead/' + id;
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  getLeadsCustomFieldsData(id: any){
    let apiurl = environment.apiURL + 'custom-fields/lead-id/' + id;
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  checkExistingEmail(data: any){
    let apiurl = environment.apiURL + 'leads/check-email-exist-or-not';
    return this.http.post(apiurl,data ,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  removeDuplicate(data: any){
    let apiurl = environment.apiURL + 'leads/remove-all-duplicate-leads-expect-current-lead';
    return this.http.post(apiurl, data, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

  getLeadLimits(){
    let url = environment.apiURL + "get-lead-limits";
    return this.http.get(url, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }
  
  updateLeadData(data:any,id:number){
    let url = environment.apiURL + "lead/update/" +id;
    return this.http.post(url, data, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }
  
  deleteLeadData(id:number){
    let url = environment.apiURL + "lead/" +id;
    return this.http.delete(url, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }

  getLeadsContactData(id: any){
    let apiurl = environment.apiURL + 'lead-contact/' + id;
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

  
  leadContactUpdate(data:any,id:number){
    let url = environment.apiURL + "lead-contact-update/" +id;
    return this.http.post(url, data, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }
  getLeadContacts(data:any){
    let url = environment.apiURL + "all-lead-contact";
    return this.http.post(url, data, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }


  leadContactDelete(id:number){
    let url = environment.apiURL + "lead-contact/" +id;
    return this.http.delete(url, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }

  createleave(data:any){
    let url = environment.apiURL+"lead-contact";
    return this.http.post(url,data,this.httpOptions)
    .pipe(
      catchError(this.errorHandler)
    )
  }

  getLeadsSingalCompanyData(id: any){
    let apiurl = environment.apiURL + 'lead-company/' + id;
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  getLeadCompanies(data:any){
    let url = environment.apiURL + "all-lead-company";
    return this.http.post(url, data, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }

  
  getDateTimeCurrencyList(){
    let apiurl = environment.apiURL + 'client/get-date-time-currency-list';
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

  leadCompanyUpdate(data:any,id:number){
    let url = environment.apiURL + "lead-company-update/" +id;
    return this.http.post(url, data, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }
  
  addLeadCompany(data:any){
    let url = environment.apiURL + "lead-company";
    return this.http.post(url, data, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }

  leadCompanyDelete(id:number){
    let url = environment.apiURL + "lead-company/" +id;
    return this.http.delete(url, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).pipe(catchError(this.errorHandler));
  }

  getLeadActivity($id:number,page:number){
    let apiurl = environment.apiURL + 'lead/activities/'+$id+'?page='+page;
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

  getRemainingEmails(){
    let apiurl = environment.apiURL + 'get-remaining-emails';
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  getCampaign(){
    let apiurl = environment.apiURL + 'campaign/get-list-without-html-and-json';
    return this.http.get(apiurl, {
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  getCampaignPerview(data:any){
    let apiurl = environment.apiURL + 'campaign/send-html-perview';
    return this.http.post(apiurl,data ,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  getCampaignSend(data:any){
    let apiurl = environment.apiURL + 'campaign/send';
    return this.http.post(apiurl,data ,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  
  addNote(data:any){
    let apiurl = environment.apiURL + 'notes';
    return this.http.post(apiurl,data ,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }
  getLeadNotes(id:any){
    let apiurl = environment.apiURL + 'leads/lead-notes/'+id;
    return this.http.get(apiurl ,{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }
    }).pipe(catchError(this.errorHandler));
  }

}
