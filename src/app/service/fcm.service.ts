import { Injectable } from '@angular/core';
import { Capacitor} from '@capacitor/core';
import { PushNotifications } from '@capacitor/push-notifications';
import { Router } from '@angular/router';
import { Device } from '@capacitor/device';
import { TokenStorageService } from './token-storage.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  constructor(private http: HttpClient,
    private tokenStorage: TokenStorageService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  initPush(){
    if(Capacitor.getPlatform() !== 'web'){
      this.registerPush();  
    }
  }

  async registerPush(){
      try{
        await PushNotifications.addListener('registration', (token:any) => {
          //alert('Registration token: '+token.value);
          if(token.value){
            this.setDeviceToken(token.value);
          }
        });
      
        await PushNotifications.addListener('registrationError', err => {
          console.error('Registration error: ', err.error);
        });
      
        await PushNotifications.addListener('pushNotificationReceived', notification => {
          console.log('Push notification received: ', notification);
        });
      
        await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
          console.log('Push notification action performed', notification.actionId, notification.inputValue);
        });

        let permStatus = await PushNotifications.checkPermissions();
    
        if (permStatus.receive === 'prompt') {
          permStatus = await PushNotifications.requestPermissions();
        }
      
        if (permStatus.receive !== 'granted') {
          throw new Error('User denied permissions!');
        }      
        await PushNotifications.register();

        const notificationList = await PushNotifications.getDeliveredNotifications();
        console.log('delivered notifications', notificationList);
      }catch(err){
        console.log(err)
      }
  }

  errorHandler(error: any) {
    let errorMessage = '';
    if (error instanceof HttpErrorResponse) {
      errorMessage = error.error;
    } else {
      errorMessage = error;
    }
    return throwError(errorMessage);
  }

  setDeviceToken(deviceToken:any){
    //alert("setDeviceToken--81");
    this.http.post( environment.apiURL +'device-token', {'token':deviceToken},{
      headers: { 'Authorization': `Bearer ${this.tokenStorage.getToken()}` }}).subscribe({
        next:(value:any)=> {
            //alert(value.message)
        },error(err) {
            alert(err.error);
        },
      });
  }

  

  async logDeviceInfo(){
    const info = await Device.getInfo();
  
    console.log(info);
  };
  
  async logBatteryInfo(){
    const info = await Device.getBatteryInfo();
  
    console.log(info);
  };
}
