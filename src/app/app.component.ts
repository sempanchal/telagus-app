import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './service/token-storage.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit{
  public appPages = [
    { title: 'Dashboard', url: '/dashboard', icon: 'home' },
    { title: 'Outbox', url: '/folder/outbox', icon: 'paper-plane' },
    { title: 'Favorites', url: '/folder/favorites', icon: 'heart' },
    { title: 'Archived', url: '/folder/archived', icon: 'archive' },
    { title: 'Trash', url: '/folder/trash', icon: 'trash' },
    { title: 'Spam', url: '/folder/spam', icon: 'warning' },
  ];

  userId:any = null;
  userClientId:any = null;
  constructor(private tokenStorage:TokenStorageService,
    private router: Router,
    private menuCtrl: MenuController,
  ) {
  }

  ngOnInit(): void {
    this.userClientId = this.tokenStorage.getUser().user.client_id;
    this.userId = this.tokenStorage.getUser().user.id;
  }

  userLogout() {
    this.menuCtrl.close();
    this.tokenStorage.signOut();
    this.router.navigateByUrl('login');
  }

}
