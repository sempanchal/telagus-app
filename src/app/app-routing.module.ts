import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/guard.guard';

const routes: Routes = [
  // {
  //   path: '',
  //   canLoad: [AuthGuard],
  //   redirectTo: 'folder/Inbox',
  //   pathMatch: 'full',
    
  // },
  {
    path: '',
    canActivate: [AuthGuard],
    loadChildren: () => import('./page/leads/leads.module').then( m => m.LeadsPageModule)
  },
  {
    path: 'dashboard',
    canActivate: [AuthGuard],
    loadChildren: () => import('./page/dashboard/dashboard.module').then( m => m.DashboardPageModule),
  },
  {
    path: 'folder/:id',
    canActivate: [AuthGuard],
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./page/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'leads',
    canActivate: [AuthGuard],
    loadChildren: () => import('./page/leads/leads.module').then( m => m.LeadsPageModule)
  },
  {
    path: 'lead-edit/:id',
    canActivate: [AuthGuard],
    loadChildren: () => import('./page/leads/lead-edit/lead-edit.module').then( m => m.LeadEditPageModule)
  },
  {
    path: 'quotes',
    canActivate: [AuthGuard],
    loadChildren: () => import('./page/quotes/quotes.module').then( m => m.QuotesPageModule)
  },
  // {
  //   path: 'quotes',
  //   canActivate: [AuthGuard],
  //   children: [
  //     {
  //       path: '',
  //       loadChildren: () => import('./page/quotes/quotes.module').then( m => m.QuotesPageModule),
  //     }, 
  //     {
  //       path: 'view-quotes/:id',
  //       loadChildren: () => import('./page/quotes/view-quote/view-quote.module').then( m => m.ViewQuotePageModule),
  //     },
  //   ],
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
