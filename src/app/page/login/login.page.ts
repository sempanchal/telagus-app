import { Component, ContentChild, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/service/login.service';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { IonInput, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment'; 
import { FcmService } from 'src/app/service/fcm.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ContentChild(IonInput) input!: IonInput;
  showPassword = false;
  logoUrl:any = environment.AWSBucketURL+'logo.svg';
  formSubmitted:boolean = false;
  loginForm!: FormGroup;
  constructor(private formBilder:FormBuilder,
    private loginApi:LoginService,
    private tokenStorage:TokenStorageService,
    private toastController: ToastController,
    private router: Router,
    private fcmService: FcmService,
  ) {

    if (this.tokenStorage.getToken()) {
      this.router.navigate(['']);
    }
  }

  ngOnInit() {
    let username = (localStorage.getItem('telagus_username'))?localStorage.getItem('telagus_username'):'';
    let password = (localStorage.getItem('telagus_password'))?localStorage.getItem('telagus_password'):'';
    let remember_me = (localStorage.getItem('remember_me'))?localStorage.getItem('remember_me'):false;
    this.loginForm = this.formBilder.group({
      email: [username, [Validators.required, Validators.email]],
      password: [password,[Validators.required]],
      remember_me: [remember_me],
    });
  }

  loginSpinner:boolean = false;
  getlogin(data:any){
    console.log(data);
    if(this.loginForm.invalid){
      this.formSubmitted = true;
    }else{
      this.loginSpinner = true;
      this.formSubmitted = false;
      this.loginApi.login(data).subscribe({
        next:(response:any)=>{
          //if(response.success){
            if(response.access_token){
              if(data.remember_me){
                localStorage.setItem('telagus_username', data.email);
                localStorage.setItem('telagus_password', data.password);
                localStorage.setItem('remember_me', data.remember_me);
              }else{
                localStorage.removeItem('telagus_username');
                localStorage.removeItem('telagus_password');
                localStorage.removeItem('remember_me');
              }
              

              this.tokenStorage.saveToken(response.access_token);
              this.tokenStorage.saveUser(response.user);
              this.tokenStorage.saveUserInfo(response.user_info);
            
              if(response?.xero_info?.token_data){
                this.tokenStorage.saveXeroInfo(JSON.parse(response.xero_info.token_data));
              }
    
              if(response?.columns_grid_of_custom_fields_info?.field_columns_json){
                var field_columns_json = JSON.parse(response?.columns_grid_of_custom_fields_info?.field_columns_json);
                localStorage.setItem('tl-custom-fields-grid-view',JSON.stringify(field_columns_json));
              }
    
              if(response?.columns_grid_of_leads_info?.field_columns_json){
                var field_columns_json = JSON.parse(response?.columns_grid_of_leads_info?.field_columns_json);
                localStorage.setItem('tl-leads-grid-view',JSON.stringify(field_columns_json));
              }
              
              if(response?.choose_module_info?.response){
                this.tokenStorage.saveChooseModuleInfo(JSON.parse(response.choose_module_info.response));
              }
    
              if(this.tokenStorage.getToken()){
                this.fcmService.initPush();
                //this.requestPermission();
                this.loginSpinner = false;
                this.router.navigate(['/leads/listing'])
              }
            }
            if(response.message){
              this.presentToast('top',response.message);
            }
          //}
        },
        error: (e) => {
          if(e.message){
            this.presentToast('top',e.message);
          }
          this.loginSpinner = false;
        }, 
        complete: () => {
          this.loginSpinner = false;
        },
      })
    }
  }

  /*pushNotification(bbtoken:any){

  this.fcmService.registerPush().then((rs: any) => {
    alert(rs);
    if('102'+rs){
      this.loginApi.setDeviceToken(rs,bbtoken).subscribe({
        next:(value:any) => {
            console.log(value);
            alert('106'+value.message);
        },
        error(err) {
            alert('error'+err);
        },
      });
    }
  });
   
    this.fcmService.logDeviceInfo();
  }*/

  showPsw:boolean = false;
  inputtype:String = 'password';
  iconName:string = 'eye';
  viewPass(event:any){
    if(this.inputtype == 'password'){
      this.inputtype = 'text';
      this.iconName = 'eye-off';
    }else{
      this.inputtype = 'password';
      this.iconName = 'eye';
    }
  }

  async presentToast(position: 'top' | 'middle' | 'bottom',msg:string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1500,
      position: position,
      color:'light',
      icon:"bug",
    });

    await toast.present();
  }
}
