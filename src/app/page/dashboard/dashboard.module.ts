import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { HeaderModule } from 'src/app/components/header/header.module';
import { TimeCardModule } from 'src/app/components/time-card/time-card.module';
import { LeadStatisticsModule } from 'src/app/components/lead-statistics/lead-statistics.module';
import { RevenueStatisticsModule } from 'src/app/components/revenue-statistics/revenue-statistics.module';
import { ConversionRatioModule } from 'src/app/components/conversion-ratio/conversion-ratio.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule,
    HeaderModule,
    TimeCardModule,
    LeadStatisticsModule,
    RevenueStatisticsModule,
    ConversionRatioModule
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule {}
