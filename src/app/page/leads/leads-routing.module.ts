import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeadsPage } from './leads.page';

const routes: Routes = [
  {
    path: '',
    component: LeadsPage
  },
  {
    path: 'listing',
    loadChildren: () => import('./listing/listing.module').then( m => m.ListingPageModule)
  },
  {
    path: 'lead-edit',
    loadChildren: () => import('./lead-edit/lead-edit.module').then( m => m.LeadEditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadsPageRoutingModule {}
