import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ListingPageRoutingModule } from './listing-routing.module';
import { ListingPage } from './listing.page';
import { NgxPaginationModule } from 'ngx-pagination';
import { HeaderModule } from 'src/app/components/header/header.module';
import { FilterPipe } from 'src/app/pipes/filter.pipe'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListingPageRoutingModule,
    NgxPaginationModule,
    HeaderModule,
    ReactiveFormsModule
  ],
  declarations: [ListingPage, FilterPipe]
})
export class ListingPageModule {}
