import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { environment } from 'src/environments/environment';
import { LeadFilterService } from 'src/app/service/lead-filter.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaginationControlsDirective } from 'ngx-pagination';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { IonModal } from '@ionic/angular';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { LeadsService } from 'src/app/service/leads.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.page.html',
  styleUrls: ['./listing.page.scss'],
})
export class ListingPage implements OnInit {
  @ViewChild('ajaxloder') ajaxloder: HeaderComponent | any;
  ajaxCall:boolean = true;
  ajaxCall1:boolean = false;

  public posts:any = [];
  public ajaxLoader:boolean = true;
  public count = 0;
  public itemsPerPage:number = 10;
  public currentPage = 1;

  //searchForm!: FormGroup;
  inputInfo!: FormGroup;
  booleanInfo!: FormGroup;
  dropdownInfo!: FormGroup;
  leadAddForm!: FormGroup;
  leadAddFormSubmit:boolean = false;
  yearInfo!: FormGroup;
  dateInfo!: FormGroup;
  sliderInfo!: FormGroup;
  currentYear: number = new Date().getFullYear();

  constructor(public tokenStorage: TokenStorageService,
    private http: HttpClient,
    private leadFilterService: LeadFilterService,
    private formBuilder:FormBuilder,
    private route: Router,
    private leadApi:LeadsService) 
    {

      this.leadAddForm = formBuilder.group({
        lead_assign_to: [''],
        lead_title: [''],
        company_name: [''],
        price: [''],
        lead_contacts: formBuilder.group({
          is_primary_contact: [1, Validators.required],
          first_name: ['', Validators.required],
          last_name: [],
          email: ['',Validators.email],
          mobile_number: ['',Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]
        })
      });
      
      this.inputInfo = formBuilder.group({
        filter_title: [''],
        filter_name: [''],
        filter_selected: ['contains', [Validators.required]],
        filter_type: ['input', [Validators.required]],
        contains: ['', [Validators.required]],
        doesnt_contain: [''],
        edit_position: [-1],
      });
      
      this.booleanInfo = formBuilder.group({
        filter_title: [''],
        filter_name: [''],
        filter_value: ['', Validators.required],
        filter_type: ['boolean', [Validators.required]],
        edit_position: [-1],
      });
      this.dropdownInfo = formBuilder.group({
        filter_title: [''],
        filter_name: [''],
        filter_value: ['', Validators.required],
        filter_type: ['dropdown', [Validators.required]],
        edit_position: [-1],
      });
      this.yearInfo = formBuilder.group({
        filter_title: [''],
        filter_name: [''],
        filter_value: ['', Validators.required],
        filter_type: ['year', [Validators.required]],
        edit_position: [-1],
      });
      this.dateInfo = formBuilder.group({
        filter_title: [''],
        filter_name: [''],
        start_date: [null],
        end_date: [''],
        filter_selected: ['is'],
        filter_value: ['today', [Validators.required]],
        filter_type: ['date', [Validators.required]],
        is_equal_to: [''],
        is_before: [''],
        is_after: [''],
        edit_position: [-1],
      });
      this.sliderInfo = formBuilder.group({
        filter_title: [''],
        filter_name: [''],
        filter_min_value: ['', Validators.required],
        filter_max_value: ['', Validators.required],
        filter_type: ['slider', [Validators.required]],
        edit_position: [-1],
      });

    }

  membersOptions:any = [];
  leadPositionOptions:any = [];
  leadPositionStageOptions:any = [];
  dateOptions: any = [];
  ngOnInit() {
    //this.assignedToteam = this.tokenStorage.getUser().user.client.users;
    this.getLeadLimits();
    this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'keyword':this.keyword});
    this.getLeadFilters();
    this.getLeadsCustomFields();
    
    if(this.tokenStorage.getUser()){
      this.tokenStorage.getUser().user.client.users.forEach((entry:any) => {
        this.membersOptions.push({name: entry.first_name + ' '+ entry.last_name, value: entry.id});
      });
    }
    this.tokenStorage.getUser().user.client.lead_position.forEach((entry:any) => {
      this.leadPositionOptions.push({name: entry.name, value: entry.id});
      entry.lead_position_stage_set.forEach((entry1:any) => {
        entry1.lead_position_stage.forEach((entry2:any) => {
          this.leadPositionStageOptions.push({name: entry2.name, value: entry2.id});
        });
      });
    });

    this.dateOptions = this.leadFilterService.getDateOptions();
    
  }

  leadLimits:any = null;
  getLeadLimits(){
    this.leadApi.getLeadLimits().subscribe({
      next:(value:any) => {
          //console.log(value);
          this.leadLimits = value;
      },
    })
  }

  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${this.tokenStorage.getToken()}`
    })
  }

  leadCustomFields:any = null;
  fieldLength:number = 0;
  getLeadsCustomFields(){
    let apiurl = environment.apiURL + 'custom-fields';
    return this.http.get(apiurl, this.httpOptions).subscribe({
      next:(value:any) => {
        if(value.custom_fields){
          this.leadCustomFields = value.custom_fields;

          this.leadCustomFields.leads.forEach((element:any) => {

            if(element.id){
              //let value = (this.leadData.custom_fields[element.id]?.value)?this.leadData.custom_fields[element.id]?.value:null;
              if (element.field_type == 'checkboxes') {
                let group: any = this.formBuilder.group({});
                element.options.forEach((option:any) => {
                  group.addControl(option.value, this.formBuilder.control(null));
                });
                this.leadAddForm.addControl('lead_custom_' + element.id, group);
              }else{
                if(element.required){
                  this.leadAddForm.addControl('lead_custom_' + element.id, this.formBuilder.control(null, Validators.required));
                }else{
                  this.leadAddForm.addControl('lead_custom_' + element.id, this.formBuilder.control(null));
                }
              }
            }
            this.fieldLength+=1;
          });
        }
        

      },
      complete:() => {
          
      },
    });
  }

  public onChange(event:any): void {
    this.currentPage = event;
    if(this.appliedFilters.length > 0){
      this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'filter':this.appliedFilters,'keyword':this.keyword});
    }else{
      this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'keyword':this.keyword});
    }
    
  }
  leadPositionID:any = null;
  getList(date:any){
    this.posts = [];
    this.ajaxLoader = true;
    let leadposition_array: any = this.tokenStorage.getUser().user.client.lead_position;
    const result = leadposition_array.find(({is_lead}:any) => is_lead == 1);
    this.leadPositionID = result.id;
    this.http.post(
      environment.apiURL + 'lead-list?lead_position_id='+result.id,date,this.httpOptions
      ).subscribe({
        next:(res:any) =>{
          this.ajaxloder.ajaxCallFunction(true);
          this.posts = res.leads;
          this.count =  res.total;
        },
        error:(err)=>{
          this.ajaxLoader = false;
          setTimeout(() => {
            this.ajaxloder.ajaxCallFunction(false);
          }, 1000);
        },
        complete:()=> {
          this.ajaxLoader = false;
          setTimeout(() => {
            this.ajaxloder.ajaxCallFunction(false);
          }, 1000);
          this.setOpen(false);
        },
      });
  }

  goToPage($event:any){
    if(this.appliedFilters.length > 0){
      this.getList({'page':$event.target.value,'limit':this.itemsPerPage,'filter':this.appliedFilters,'keyword':this.keyword});
    }else{
      this.getList({'page':$event.target.value,'limit':this.itemsPerPage,'keyword':this.keyword});
    }  
  }

  isModalOpen = false;
  searchText:any = null;
  setOpen(isOpen: boolean) {
    this.isModalOpen = isOpen;
  }
  leadFilters: any = null;
  getLeadFilters() {
    this.leadFilters = this.leadFilterService.getLeadFilters();
  }

  currentFilterElement: any;
  setCurrentFilterElement(elemName:any){
    
    let edit_position = -1
    loop1: //adding a label to break both the loops
    for (let i of this.leadFilters) {
      for (let filter of i.filter_group.filters) {
        if (elemName == filter.filter_name) {
          this.currentFilterElement = filter;
          break loop1;
        }
      }
    }
    /*
    if(this.filterAlreadyApplied(this.currentFilterElement)){
      this.setCurrentFilterElementWithValues(this.getAlreadyAppliedFilterWithValues(this.currentFilterElement))
      return;
    }*/

    switch (this.currentFilterElement.filter_name) {
      case "company_created_at":
        this.currentFilterElement.filter_name = "lead_companies.created_at";
        break;
      case "company_updated_at":
        this.currentFilterElement.filter_name = "lead_companies.updated_at";
        break;
      case "lead_created_at":
        this.currentFilterElement.filter_name = "leads.created_at";
        break;
      default:
        this.currentFilterElement.filter_name = this.currentFilterElement.filter_name;
    }
    
    if (this.currentFilterElement.filter_type == "input") {
      this.inputInfo.patchValue({
        'filter_type': "input",
        'filter_selected': "contains",
        'filter_name': this.currentFilterElement.filter_name,
        'filter_title': this.currentFilterElement.filter_title,
        'edit_position': edit_position
      });
      this.containsCheckedFunc();
    } else if (this.currentFilterElement.filter_type == "boolean") {
      this.booleanInfo.patchValue({
        'filter_type': "boolean",
        'filter_value': "",
        'filter_name': this.currentFilterElement.filter_name,
        'filter_title': this.currentFilterElement.filter_title,
        'edit_position': edit_position
      });
    } else if (this.currentFilterElement.filter_type == "dropdown") {
      this.dropdownInfo.patchValue({
        'filter_type': "dropdown",
        'filter_value': "",
        'filter_name': this.currentFilterElement.filter_name,
        'filter_title': this.currentFilterElement.filter_title,
        'edit_position': edit_position
      });
    } else if (this.currentFilterElement.filter_type == "year") {
      this.yearInfo.patchValue({
        'filter_type': "year",
        'filter_value': this.currentYear,
        'filter_name': this.currentFilterElement.filter_name,
        'filter_title': this.currentFilterElement.filter_title,
        'edit_position': edit_position
      });
    } else if (this.currentFilterElement.filter_type == "date") { 
      this.dateInfo.patchValue({
        'filter_type': "date",
        'filter_value': "today",
        'filter_selected': "is",
        'filter_name': this.currentFilterElement.filter_name,
        'filter_title': this.currentFilterElement.filter_title,
        'edit_position': edit_position,  
      });
      this.dateIsCheckedFunc();
    } else if (this.currentFilterElement.filter_type == "slider") {
      this.sliderInfo.patchValue({
        'filter_type': "slider",
        'filter_min_value': "0",
        'filter_max_value': "1000",
        'filter_name': this.currentFilterElement.filter_name,
        'filter_title': this.currentFilterElement.filter_title,
        'edit_position': edit_position
      });
    }
    this.hideAppliedFiltersFunc();
    this.hideLeadFiltersFunc();
    this.showFilterElementFunc();
  }

  containsChecked: boolean = true;
  doesntContainChecked: boolean = false;
  containsCheckedFunc() {
    this.containsChecked = true;
    this.doesntContainChecked = false;
  }

  showAppliedFilters: boolean = false;
  hideAppliedFiltersFunc() {
    this.showAppliedFilters = false;
  }

  showLeadFilters: boolean = true;
  hideLeadFiltersFunc() {
    this.showLeadFilters = false;
  }

  showFilterElement: boolean = false;
  showFilterElementFunc() {
    this.hideAppliedFiltersFunc()
    this.hideLeadFiltersFunc();
    this.showFilterElement = true;
  }

  dateIsChecked: boolean = true;
  dateIsEqualToChecked: boolean = false;
  dateIsBeforeChecked: boolean = false;
  dateIsAfterChecked: boolean = false;
  dateIsBetweenChecked: boolean = false;
  dateIsKnownChecked: boolean = false;
  dateIsUnknownChecked: boolean = false;

  dateIsCheckedFunc() {
    this.dateIsChecked = true;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = false;
  }

  dateIsEqualToCheckedFunc() {
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = true;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = false;
  }

  dateIsBeforeCheckedFunc() {
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = true;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = false;
  }

  dateIsAfterCheckedFunc() {
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = true;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = false;
  }

  dateIsBetweenCheckedFunc() {
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = true;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = false;
  }

  dateIsKnownCheckedFunc() {
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = true;
    this.dateIsUnknownChecked = false;
  }

  dateIsUnknownCheckedFunc() {
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = true;
  }

  appliedFilters: any = [];
  showAppliedFiltersFunc() {
    this.searchText = null;
    this.inputInfo.reset();
    this.dropdownInfo.reset();
    this.yearInfo.reset();
    this.dateInfo.reset();
    this.sliderInfo.reset();
    this.hideFilterElementFunc();
    this.hideLeadFiltersFunc();
    this.showAppliedFilters = true;
  }

  hideFilterElementFunc() {
    this.showFilterElement = false;
  }

  isKnownOrUnknownCheckedFunc() {
    this.containsChecked = false;
    this.doesntContainChecked = false;
  }

  doesntContainCheckedFunc() {
    this.containsChecked = false;
    this.doesntContainChecked = true;
  }

  showLeadFiltersFunc() {
    //to reset the search filter for searchable fields
    this.searchText = null;
    //this.searchForm.reset();
    this.inputInfo.reset();
    this.hideFilterElementFunc();
    this.hideAppliedFiltersFunc();
    this.showLeadFilters = true;
  }

  infoSubmit(general:any){
    this.currentPage = 1;

    if(this.appliedFilters.length > 0){
      const john = this.appliedFilters.find((filter:any) => filter.filter_name === general.filter_name);
      const removeIndex = this.appliedFilters.findIndex( (item:any) => item.filter_name === general.filter_name );
      if (john) {
        this.appliedFilters.splice( removeIndex, 1 );
      }
    }

    if(general.filter_type == 'date' && general.filter_selected == 'is'){
        let parsedValue = this.leadFilterService.getDateValueFromStringValue(general.filter_value);
        general.date_between = parsedValue;
    }
    this.appliedFilters.push(general);

    if(this.appliedFilters.length > 0){
      this.setOpen(false);
      this.getList({'page':1,'limit':this.itemsPerPage,'filter':this.appliedFilters,'keyword':this.keyword});
    }
    this.showAppliedFiltersFunc();
  }

  datePick($date:any,event:any){
    let dateSelected = this.formatDate($date.detail.value);
    if(event == 'is_equal_to'){
      this.dateInfo.patchValue({
        start_date: '',
        end_date: '',
        is_equal_to: dateSelected,
        is_before: '',
        is_after: '',
      });
    }
    else if(event == 'is_before'){
      this.dateInfo.patchValue({
        start_date: '',
        end_date: '',
        is_equal_to: '',
        is_before: dateSelected,
        is_after: '',
      });
    }
    else if(event == 'is_after'){
      this.dateInfo.patchValue({
        start_date: '',
        end_date: '',
        is_equal_to: '',
        is_before: '',
        is_after: dateSelected,
      });
    }
    else if(event == 'start_date' || event == 'end_date'){
      this.dateInfo.patchValue({
        is_equal_to: '',
        is_before: '',
        is_after: '',
      });
      if(event == 'start_date'){ this.dateInfo.patchValue({ start_date: dateSelected }); }
      if(event == 'start_date'){ this.dateInfo.patchValue({ end_date: dateSelected }); }
    }
    else{
      this.dateInfo.patchValue({
        start_date: '',
        end_date: '',
        is_equal_to: '',
        is_before: '',
        is_after: '',
      });
    }
    //'is_equal_to': (new Date("2024-04-19T18:03:40.887")).toJSON(), 
  }

  formatDate(date:any) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }

  clearAllFilters() {
    this.appliedFilters = [];
    this.dateIsChecked = false;
    this.dateIsEqualToChecked = false;
    this.dateIsBeforeChecked = false;
    this.dateIsAfterChecked = false;
    this.dateIsBetweenChecked = false;
    this.dateIsKnownChecked = false;
    this.dateIsUnknownChecked = false;
    this.containsChecked = false;
    this.doesntContainChecked = false;
    this.currentPage = 1;
    this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'keyword':this.keyword});
  }

  addLeadModal:boolean = false
  cancel(isOpen: boolean) {
    this.addLeadModal = isOpen;
  }
  open(isOpen: boolean) {
    this.addLeadModal = isOpen;
  }

  addNewLeadData(lead:any){
    if(this.leadAddForm.invalid){
      this.leadAddFormSubmit = true;
      //console.log(this.leadAddForm.controls['lead_contacts'].get('first_name')?.errors?.['required']);
      Swal.fire({
        heightAuto: false,
        title: '',
        text: "Please fill all the required fields.",
        icon: 'warning',
      });
    }else{
      this.addLeadModal = false;
      this.leadAddFormSubmit = false;
      //console.log('578',lead);
      this.addLead(lead);
    }
  }

  addLead(lead:any) {
    let postdata = {
      assigned_to: lead.lead_assign_to,
      price: lead.price,
      lead_title: lead.lead_title,
      lead_position_id: this.leadPositionID,
      lead_companies: [{
        'company_name': lead.company_name,
        'is_primary_company': 1
      }]
    }
    const formData: FormData = new FormData();
    if (lead.lead_assign_to) {
      formData.append('assigned_to', lead.lead_assign_to);
    }

    //let contactInc = 0;
    //lead.lead_contacts.forEach((element:any) => {
      formData.append("lead_contacts[0][first_name]", lead.lead_contacts.first_name);
      formData.append("lead_contacts[0][last_name]", lead.lead_contacts.last_name);
      formData.append("lead_contacts[0][email]", lead.lead_contacts.email);
      formData.append("lead_contacts[0][mobile_number]", lead.lead_contacts.mobile_number);
      formData.append("lead_contacts[0][is_primary_contact]", lead.lead_contacts.is_primary_contact);
      //contactInc++;
    //});
    if (lead.price) {
      formData.append('price', lead.price);
    }
    if (lead.lead_title) {
      formData.append('lead_title', lead.lead_title);
    }
    if (this.leadPositionID) {
      formData.append('lead_position_id', this.leadPositionID);
    }
    if (lead.company_name) {
      let lead_companies: any = {
        'company_name': lead.company_name,
        'is_primary_company': 1
      };
      formData.append('lead_companies[0][company_name]', lead.company_name);
      formData.append('lead_companies[0][is_primary_company]', '1');
    }
    
    if (this.leadCustomFields.leads) {
      let field_name: any;
      let formdataIdFieldName: any;
      let formdataValueFieldName: any;
      let formdataInputFieldName: any;
      let inc = 0;
      this.leadCustomFields.leads.forEach((element:any) => {
        field_name = "lead_custom_" + element.id;
        formdataIdFieldName = "custom_fields[" + inc + "][id]";
        formdataValueFieldName = "custom_fields[" + inc + "][value]";
        formdataInputFieldName = "custom_fields[" + inc + "][field_type]";
        if (element.field_type == 'checkboxes') {
          let ids: string[] = [];
          for (var key in this.leadAddForm.controls) {
            if (field_name == key && this.leadAddForm.controls[key].value) {
              for (var checkVal in this.leadAddForm.controls[key].value) {
                if (this.leadAddForm.controls[key].value[checkVal]) {
                  ids.push(checkVal);
                }
              }
            }
          }
          if (ids.length) {
            formData.append(formdataIdFieldName, element.id);
            formData.append(formdataValueFieldName, ids.join());
            formData.append(formdataInputFieldName, element.field_type);
          }
        } 
        /*else if (element.field_type == 'file') {
          if (this.leadCustomFieldFileToUpload[field_name]) {
            formData.append(formdataIdFieldName, element.id);
            formData.append(formdataValueFieldName, this.leadCustomFieldFileToUpload[field_name]);
            formData.append(formdataInputFieldName, element.field_type);
          }
        } */
        else {
          if (this.leadAddForm.controls[field_name].value) {
            formData.append(formdataIdFieldName, element.id);
            formData.append(formdataValueFieldName, this.leadAddForm.controls[field_name].value);
            formData.append(formdataInputFieldName, element.field_type);
          }
        }
        inc++;
      });
    }
    let apiurl = environment.apiURL + "lead";
    this.ajaxCall1 = true;
    this.http.post(apiurl, formData, {
      headers: {'Authorization': `Bearer ${this.tokenStorage.getToken()}`}
    }).subscribe({
      next:(res: any)=> {
        //console.log(res);
        if (res.message) {
          Swal.fire({
            heightAuto: false,
            title: 'Success',
            text: res.message,
            icon: 'success',
            confirmButtonText: 'View Lead',
            confirmButtonColor: '#3085d6',
            showDenyButton: true,
            denyButtonText: 'Ok',
          }).then((result) => {
            this.leadAddForm.reset();
            this.leadAddForm.controls['lead_contacts'].get('is_primary_contact')?.setValue(1);
            this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'keyword':this.keyword});
            if (result.isConfirmed) {
              this.route.navigateByUrl('lead-edit/'+res.lead.id);
            }
          });
        }
      },
      error:(err:any) =>{
        console.log('Add lead error:: ', err);
      },
      complete:()=> {
        this.ajaxCall1 = false;
      },
    });
  }

  keyword:any = null;
  onSearchInput(keyword:any){
    //console.log(keyword.detail.value);
    if(keyword.detail.value.length > 3){
      this.keyword = keyword.detail.value;
      this.getList({'page':1,'limit':this.itemsPerPage,'keyword':this.keyword});
    }else if(keyword.detail.value.length == 0){
      this.keyword = null;
      this.getList({'page':1,'limit':this.itemsPerPage,'keyword':this.keyword});
    }else{
      this.keyword = null;
    }
  }

  delectLead($id:number){
    Swal.fire({
      heightAuto: false,
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      showCancelButton: true,
      confirmButtonText: 'Yes, Delete It!.',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#23272b',
      customClass: {
        actions: 'my-actions',
        cancelButton: 'order-2 right-gap',
        confirmButton: 'order-1',
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.leadApi.deleteLeadData($id).subscribe({
          next:(value:any) => {
            Swal.fire({
              heightAuto: false,
              title: 'Success',
              text: 'Record has been delete sucessfully.',
              icon: 'success',
            }).then(() => {
              this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'keyword':this.keyword});
            });
          },
        });
      }
    });
    
  }

  removeFilterItem(data:any,index:number){
    if(this.appliedFilters.length == 1){
      this.appliedFilters = [];
      this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'keyword':this.keyword});
    }else{
      this.appliedFilters.splice(index,1);
      this.getList({'page':this.currentPage,'limit':this.itemsPerPage,'filter':this.appliedFilters,'keyword':this.keyword});
    }
   
  }
}
