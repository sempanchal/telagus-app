import { Component, OnInit } from '@angular/core';
import { LeadsService } from 'src/app/service/leads.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent  implements OnInit {

  addNoteForm!: FormGroup;
  addNoteFormErr:boolean = false;
  constructor( private leadApi: LeadsService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public helper:HelperService
  ) {

      this.addNoteForm = this.fb.group({
        notes_comment:['', [Validators.required]],
      });
    }

  ngOnInit() {
    this.getLeadNotes();
  }

  addNote(data:any){
    if(this.addNoteForm.invalid){
      this.addNoteFormErr = true;
    }else{
      this.addNoteFormErr = false;
      let apiData = {
        lead_id:this.route.snapshot.params['id'],
        title:"Notes",
        description:data.notes_comment,
      };
      this.leadApi.addNote(apiData).subscribe({
        next:(value:any) => {
            console.log(value.message);
            Swal.fire({
              heightAuto: false,
              title: value.message,
              text: "Note has been added successfully.",
              icon: 'success',
            });
        },
        complete:() =>{
          this.addNoteForm.reset();
          this.getLeadNotes();
        },
      })
    }
  }

  lead_notes:any = [];
  ajaxLoader:boolean = true;
  getLeadNotes(){
    this.leadApi.getLeadNotes(this.route.snapshot.params['id']).subscribe({
      next:(res:any) => {
          console.log('53',res);
          this.lead_notes = res.lead_notes;
      },
      error:(err) =>{
        this.ajaxLoader = false;
      },
      complete:() =>{
          this.ajaxLoader = false;
      }
    })
  }

}
