import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonModal } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LeadsService } from 'src/app/service/leads.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
})
export class CompaniesComponent  implements OnInit {
  @ViewChild(IonModal) custModal!: IonModal;
  ajaxCall = false;
  popupTitle:any = "Edit Company";
  leadID:any;

  leadCompanies:any = null;
  formSubmit:boolean = false;
  leadCompanyEditForm!: FormGroup;
  constructor(
    private fb: FormBuilder,
    private leadApi: LeadsService,
    private route: ActivatedRoute,
  ) { 
    this.leadID = this.route.snapshot.params['id'];
    this.leadCompanyEditForm = this.fb.group({
      id:[''],
      lead_id:[this.leadID],
      company_name:['',[Validators.required]],
      company_service:[''],
      company_website:[''],
      company_email:[''],
      phone_number:[''],
      street_address:[''],
      street_address2:[''],
      city:[''],
      state:[''],
      postcode:[''],
      number_of_employees:[''],
      annual_revenue:[''],
      time_zone:[''],
      year_founded:[''],
    });
  }


  ngOnInit() {
    this.getLeadCompanies();
    this.getDateTimeCurrencyList();
  }

  timezone:any = null;
  getDateTimeCurrencyList(){
    this.leadApi.getDateTimeCurrencyList().subscribe({
      next:(value:any) => {
          this.timezone = value.timezone;
      },
    });
  }
  ajaxLoader:boolean = true;
  getLeadCompanies(){
    this.leadApi.getLeadCompanies({'lead_id':this.route.snapshot.params['id']}).subscribe({
      next:(value:any) => {
          this.leadCompanies = value.companies;
      },
      error:(err) =>{
        this.ajaxLoader = false;
      },
      complete:() =>{
          this.ajaxLoader = false;
      },
    });
  }

  open(isOpen: boolean,id:number = 0) {
    if(id > 0){
     
      this.leadApi.getLeadsSingalCompanyData(id).subscribe({
        next:(value:any) => {
          this.leadCompanyEditForm.patchValue({
            id:value.lead_company.id,
            lead_id:this.leadID,
            company_name:value.lead_company.company_name,
            company_service:value.lead_company.company_service,
            company_website:value.lead_company.company_website,
            company_email:value.lead_company.company_email,
            phone_number:value.lead_company.phone_number,
            street_address:value.lead_company.street_address,
            street_address2:value.lead_company.street_address2,
            city:value.lead_company.city,
            state:value.lead_company.state,
            postcode:value.lead_company.postcode,
            number_of_employees:value.lead_company.number_of_employees,
            annual_revenue:value.lead_company.annual_revenue,
            time_zone:value.lead_company.time_zone,
            year_founded:value.lead_company.year_founded,
          });
        },
        complete:() => {
          this.popupTitle = "Edit Company";
          this.custModal.isOpen = isOpen;
        }
      })
    }else{
      this.popupTitle = "Add Company";
      this.leadCompanyEditForm.reset();
      this.leadCompanyEditForm.patchValue({
        lead_id:this.leadID,
      });
      this.custModal.isOpen = isOpen;
    }
    
  }

  cancel(isOpen: boolean) {
    this.custModal.isOpen = isOpen;
  }

  updateALeadCompanyData(data:any){
    
    if(this.leadCompanyEditForm.invalid){
      this.formSubmit = true;
      Swal.fire({
        heightAuto: false,
        title: '',
        text: "Please fill all the required fields.",
        icon: 'warning',
      });
    }else{
      this.formSubmit = false;
      if(data.id > 0){
        this.leadApi.leadCompanyUpdate(data,data.id).subscribe({
          next:(value:any) => {
            Swal.fire({
              heightAuto: false,
              title: 'Updated',
              text: "Company has been updated sucessfully.",
              icon: 'success',
            });
            
            this.custModal.isOpen = false;
            this.leadCompanyEditForm.reset();
            this.leadCompanyEditForm.patchValue({
              lead_id:this.leadID,
            });
          },
          complete:() =>{
            this.getLeadCompanies();
          },
        });
      }else{
        this.leadApi.addLeadCompany(data).subscribe({
          next:(value:any) => {
            console.log('150',value);
            Swal.fire({
              heightAuto: false,
              title: 'Added',
              text: "Company has been added successfully.",
              icon: 'success',
            });
          },
          complete:() =>{
            this.leadCompanyEditForm.reset();
            this.leadCompanyEditForm.patchValue({
              lead_id:this.leadID,
            });
            this.getLeadCompanies();
            this.custModal.isOpen = false;
          },
        });
      }
    }
  }

  deleteCompany(id:number){
    Swal.fire({
      heightAuto: false,
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete it!'

    }).then((result) => {
      if (result.isConfirmed) {
        this.leadApi.leadCompanyDelete(id).subscribe({
          next:(value:any) => {
            Swal.fire({
              heightAuto: false,
              title: 'Delete',
              text: "Company has been deleted sucessfully.",
              icon: 'success',
            });
          },
          complete:() => {
            this.getLeadCompanies();
          },
        })
      }
    })
  }

}
