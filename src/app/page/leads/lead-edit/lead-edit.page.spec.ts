import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LeadEditPage } from './lead-edit.page';

describe('LeadEditPage', () => {
  let component: LeadEditPage;
  let fixture: ComponentFixture<LeadEditPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LeadEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
