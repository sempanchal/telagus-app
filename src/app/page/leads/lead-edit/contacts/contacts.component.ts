import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { initFlowbite } from 'flowbite';
import { IonModal } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LeadsService } from 'src/app/service/leads.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
})
export class ContactsComponent  implements OnInit {
  //@Input() contacts:any;
  @ViewChild(IonModal) custModal!: IonModal;
  ajaxCall = false;
  popupTitle:any = "Edit Contact";

  leadContacts:any = null;
  leadContactEditForm!: FormGroup;
  constructor(
    private fb: FormBuilder,
    private leadApi: LeadsService,
    private route: ActivatedRoute,
  ) { 
    this.leadContactEditForm = this.fb.group({
      id:[''],
      lead_id:[''],
      first_name:[''],
      last_name:[''],
      known_as:[''],
      email:[''],
      mobile_number:[''],
      gender:[''],
      job_title:[''],
      lead_company_id:[''],
      address_type:[''],
      street_address:[''],
      street_address2:[''],
      locality:[''],
      city:[''],
      state:[''],
      country:[''],
      postcode:[''],
      is_primary:[''],
    });

  }

  ngOnInit() {
    // setTimeout(function(){
    //   initFlowbite();
    // },700);
    //initFlowbite();
    // this.leadContacts = this.contacts.lead_contacts;
    this.getLeadContacts();
  }
  ajaxLoader:boolean = true;
  getLeadContacts(){
    this.leadApi.getLeadContacts({'lead_id':this.route.snapshot.params['id']}).subscribe({
      next:(value:any) => {
          this.leadContacts = value.contacts;
      },
      error:(err) =>{
        this.ajaxLoader = false;
      },
      complete:() =>{
          this.ajaxLoader = false;
      },
    });
  }

  open(isOpen: boolean,id:number = 0) {
    if(id > 0){
      this.leadApi.getLeadsContactData(id).subscribe({
        next:(value:any) => {
            console.log('72',value);
            let contact = value.lead_contact
            this.leadContactEditForm.patchValue({
              id:contact.id,
              lead_id:contact.lead_id,
              first_name:contact.first_name,
              last_name:contact.last_name,
              known_as:contact.known_as,
              email:contact.email,
              mobile_number:contact.mobile_number,
              gender:contact.gender,
              job_title:contact.job_title,
              lead_company_id:contact.lead_company_id,
              address_type:(contact.lead_contact_addresses)?contact.lead_contact_addresses.address_type:null,
              street_address:(contact.lead_contact_addresses)?contact.lead_contact_addresses.street_address:null,
              street_address2:(contact.lead_contact_addresses)?contact.lead_contact_addresses.street_address2:null,
              locality:(contact.lead_contact_addresses)?contact.lead_contact_addresses.locality:null,
              city:(contact.lead_contact_addresses)?contact.lead_contact_addresses.city:null,
              state:(contact.lead_contact_addresses)?contact.lead_contact_addresses.state:null,
              country:(contact.lead_contact_addresses)?contact.lead_contact_addresses.country:null,
              postcode:(contact.lead_contact_addresses)?contact.lead_contact_addresses.postcode:null,
              is_primary:String(contact.is_primary_contact),
            });
        },
        complete:() => {
          this.popupTitle = "Edit Contact";
          this.custModal.isOpen = isOpen;
        },
      })
    }else{
      this.leadContactEditForm.reset();
      this.leadContactEditForm.patchValue({
        lead_id:this.route.snapshot.params['id']
      });
      this.popupTitle = "Add Contact";
      this.custModal.isOpen = isOpen;
    }
    
  }

  cancel(isOpen: boolean) {
    this.custModal.isOpen = isOpen;
  }

  confirm(isOpen: boolean) {
    this.custModal.isOpen = isOpen;
    //this.modal.dismiss(this.name, 'confirm');
  }

  updateALeadContactData(data:any){
    let param = {
      email: data.email,
      lead_id: this.route.snapshot.params['id']
    }
    this.leadApi.checkExistingEmail(param).subscribe({ 
      next:(res:any)=>{
        if(res.is_email){
          Swal.fire({
            heightAuto: false,
            title: 'Opps!',
            text: "This Email is already existing.",
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes, Save it.',
            denyButtonText: 'Remove All Old Duplicates?',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#23272b',
            customClass: {
              actions: 'my-actions',
              cancelButton: 'order-3 right-gap',
              confirmButton: 'order-2',
              denyButton: 'order-1',
            }
          }).then((result) => {
            if (result.isConfirmed) {
              this.updateContact(data,data.id);
            } else if (result.dismiss === Swal.DismissReason.cancel) {
              Swal.fire({
                heightAuto: false,
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Remove it!'

              }).then((result) => {
                let param = {
                  email: data.email,
                  lead_id: this.route.snapshot.params['id']
                }
                this.leadApi.removeDuplicate(param).subscribe({
                  next:(res:any)=>{
                    Swal.fire({
                      heightAuto: false,
                      title: 'Updated',
                      text: "Duplicate record has been deleted sucessfully.",
                      icon: 'success',
                    });
                  }
                });
              });
            }
          })
        }else{
          this.updateContact(data,data.id);
        }
      }
    });
  }

  updateContact(data:any,id:number){
    if(id > 0){
      this.leadApi.leadContactUpdate(data,id).subscribe({
        next:(res:any) => {
          Swal.fire({
            heightAuto: false,
            title: 'Updated',
            text: "Contact has been updated sucessfully.",
            icon: 'success',
          });
          this.custModal.isOpen = false;
        },
        complete:() => {
          this.leadContactEditForm.reset();
          this.leadContactEditForm.patchValue({
            lead_id:this.route.snapshot.params['id']
          });
          this.getLeadContacts();
        },
      })
    }else{
      this.leadApi.createleave(data).subscribe({
        next:(res:any) => {
          Swal.fire({
            heightAuto: false,
            title: 'Added',
            text: "Contact has been added sucessfully.",
            icon: 'success',
          });
          this.custModal.isOpen = false;
        },
        complete:() => {
          this.leadContactEditForm.reset();
          this.leadContactEditForm.patchValue({
            lead_id:this.route.snapshot.params['id']
          });
          this.getLeadContacts();
        },
      })
    }
  }

  deleteContact(id:number){
    Swal.fire({
      heightAuto: false,
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete it!'

    }).then((result) => {
      if (result.isConfirmed) {
        this.leadApi.leadContactDelete(id).subscribe({
          next:(value:any) => {
            Swal.fire({
              heightAuto: false,
              title: 'Delete',
              text: "Contact has been deleted sucessfully.",
              icon: 'success',
            });
          },
          complete:() => {
            this.getLeadContacts();
          },
        })
      }
    })
  }

}
