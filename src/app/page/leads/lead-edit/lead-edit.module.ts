import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HeaderModule } from 'src/app/components/header/header.module';
import { IonicModule } from '@ionic/angular';
import { LeadEditPageRoutingModule } from './lead-edit-routing.module';
import { LeadEditPage } from './lead-edit.page';
import { ContactsComponent } from './contacts/contacts.component';
import { CompaniesComponent } from './companies/companies.component';
import { ActivitiesComponent } from './activities/activities.component';
import { StringReplacePipe } from 'src/app/pipes/string-replace.pipe';
import { LeadCampaignComponent } from './lead-campaign/lead-campaign.component';
import { NotesComponent } from './notes/notes.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderModule,
    IonicModule,
    LeadEditPageRoutingModule,
  ],
  declarations: [LeadEditPage,ContactsComponent,CompaniesComponent,ActivitiesComponent,StringReplacePipe,LeadCampaignComponent,NotesComponent]
})
export class LeadEditPageModule {}
