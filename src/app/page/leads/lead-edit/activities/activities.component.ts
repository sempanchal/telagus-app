import { Component, OnInit } from '@angular/core';
import { LeadsService } from 'src/app/service/leads.service';
import { ActivatedRoute } from '@angular/router';
import { concat } from 'lodash';
import { filter } from 'lodash';
import * as moment from 'moment';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import * as Plyr from "plyr";

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss'],
})
export class ActivitiesComponent  implements OnInit {

  leadID:number;
  page:number = 1;
  totalPage:number = 1;
  activities: any = [];
  players: Plyr[] = [];
  ajaxLoader:any = false;
  placeholderAvatar:any = '';
  noRecords: boolean = false;
  isLastPage: boolean = false;
  audiopath ?: any = [];
  audio: any;
  swithoop: boolean = false;
  audioSources: any = [];

  constructor(private leadApi: LeadsService,
    private route: ActivatedRoute,
    public tokenStorage:TokenStorageService,) { 
      this.leadID = this.route.snapshot.params['id'];
      this.placeholderAvatar=tokenStorage.getUserInfo().avatar;
    }

  ngOnInit() {
    this.getLeadActivity();
  }


  getLeadActivity(page=0){
    if(page > 0){
      this.page++;
      //alert(this.page);
    }
    this.ajaxLoader = true;
    this.leadApi.getLeadActivity(this.leadID,this.page).subscribe({
      next:(res:any) => {
        if(res['success']){
          for(let activity of res['activities']){
            if(activity.event == 'updated'){
              activity= this.convertPropsToArray(activity)
            }
          }
          this.activities = concat(this.activities, res['activities']);
          console.log(this.activities);
          this.totalPage = res['pages'];
        }
      },
      error:(err) =>{
        this.ajaxLoader = false;
      },
      complete:()=> {
        this.ajaxLoader = false;
      },
    })
  }

  loadMoreWithoutParams(page:number){
    //alert(page)
    this.getLeadActivity(page);
  }

  convertPropsToArray(activities:any){
    let evilResponseProps = Object.keys(activities.properties.attributes);
    let evilResponseValues = Object.values(activities.properties.attributes);
    let evilResponseProps1 = Object.keys(activities.properties.old);
    let evilResponseValues1 = Object.values(activities.properties.old);
    let goodResponse1:any = [];
    let goodResponse2:any = [];
    for (let index in evilResponseProps) {
      let tempArray:any=[];
      tempArray[evilResponseProps[index]]=evilResponseValues[index];
      goodResponse1.push(tempArray);
    }
    for (let index1 in evilResponseProps1) {
      let tempArray1:any=[];
      tempArray1[evilResponseProps1[index1]]=evilResponseValues1[index1];
      goodResponse2.push(tempArray1);
    }
    activities.properties.attributes=goodResponse1;
    activities.properties.old=goodResponse2;
    return activities;
  }

  dateToFromNowDaily( myDate:any ) {
    // get from-now for this date
    var fromNow = moment( myDate ).format("D MMM YYYY hh:mm A");
    var time = moment( myDate ).format("hh:mm A");

    // ensure the date is displayed with today and yesterday
    return moment( myDate ).calendar( null, {
      // when the date is closer, specify custom values
      lastDay:  '[Yesterday at '+time+']',
      sameDay:  '[Today at '+time+']',
      // when the date is further away, use from-now functionality
      sameElse: function () {
        return '['+fromNow+']';
      }
    });
  }

  playaudio(mediaURL:any, e:any) {
    this.stopAllAudio();
    let activity_div = document.querySelectorAll('#activity-div');
    let div = e.target.closest('#activity-div');
    this.audioSources = [];
    let index = -1;
    for (let i = 0; i < activity_div.length; i++) {
      activity_div[i].classList.remove('show_player');
      if (activity_div[i] == div) {
        index = i;
      }
    }
    div.classList.add('show_player');
    let audioExists = div.querySelector('audio');
    if (!audioExists) {
      this.audio = mediaURL;
      this.swithoop = true;
      this.audiopath = this.audio;
      let audioElement = document.createElement("audio");
      let sourceElement1 = document.createElement("source");
      let sourceElement2 = document.createElement("source");
      let sourceElement3 = document.createElement("source");
      sourceElement1.setAttribute("src", this.audiopath + '.wav');
      sourceElement1.setAttribute("type", 'audio/x-wav');
      sourceElement2.setAttribute("src", this.audiopath + '.mp3');
      sourceElement2.setAttribute("type", 'audio/mp3');
      sourceElement3.setAttribute("src", this.audiopath + '.ogg');
      sourceElement3.setAttribute("type", 'audio/ogg');
      audioElement.setAttribute("controls", '');
      audioElement.appendChild(sourceElement1);
      audioElement.appendChild(sourceElement2);
      audioElement.appendChild(sourceElement3);
      let span = div.querySelector('.show-player-div');
      span.insertBefore(audioElement, span.querySelector('.close-plyr'));
      span.querySelector('.loading').classList.add('hide');
      span.querySelector('.close-plyr').classList.remove('hide');
      let player = new Plyr(audioElement, {captions: {active: true}});
      if (index != -1) {
        this.players[index] = player;
        this.players[index].play();
      }
    } else {
      this.players[index].play();
    }
  }

  stopAllAudio() {
    for (let tempPlayer of this.players) {
      if (tempPlayer) {
        tempPlayer.stop();
      }
    }
  }

  hidePlayer(e:any) {
    let div = e.target.closest('#activity-div');
    div.classList.remove('show_player');
    this.stopAllAudio();
  }

  humanizeString(str:any) {
    var i, frags = str.split('_');
    for (i=0; i<frags.length; i++) {
      frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
    }
    return frags.join(' ');
  }

  getRemovedCollaborators(oldCollaborators:any, newCollaborators:any){
    /* code to get list of collaborators that are removed */
    oldCollaborators=JSON.parse(oldCollaborators);
    newCollaborators=JSON.parse(newCollaborators);
    let result= filter(oldCollaborators, function (o) {
      return !newCollaborators.includes(o);
    });
    return result;
  }

  getAddedCollaborators(oldCollaborators:any, newCollaborators:any){
    /* code to get list of collaborators that are added */
    oldCollaborators=JSON.parse(oldCollaborators);
    newCollaborators=JSON.parse(newCollaborators);
    let result= filter(newCollaborators, function (o) {
      return !oldCollaborators.includes(o);
    });
    return result;
  }
}

