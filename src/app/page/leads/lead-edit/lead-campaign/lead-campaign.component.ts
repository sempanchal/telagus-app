import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LeadsService } from 'src/app/service/leads.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-lead-campaign',
  templateUrl: './lead-campaign.component.html',
  styleUrls: ['./lead-campaign.component.scss'],
})
export class LeadCampaignComponent  implements OnInit {
  leadContacts:any = [];
  campaigns:any = [];
  sendCampaignForm!: FormGroup;
  sendCampaignFormErr:boolean = false;
  constructor( private leadApi: LeadsService,
    private route: ActivatedRoute,
    private fb: FormBuilder) {
      this.sendCampaignForm = this.fb.group({
        contact:['', [Validators.required]],
        campaign_id:['', [Validators.required]],
        bcc:[''],
        date:[''],
        time:[''],
      })
    }

  ngOnInit() {
    this.getLeadContacts();
    this.getCampaign();
  }

  getLeadContacts(){
    this.leadApi.getLeadContacts({'lead_id':this.route.snapshot.params['id']}).subscribe({
      next:(value:any) => {
          this.leadContacts = value.contacts;
      },
    });
  }

  getCampaign(){
    this.leadApi.getCampaign().subscribe({
      next:(value:any) => {
        console.log('31',value);
         this.campaigns = value
      },
    });
  }

  contactId:number = 0;
  changeContact(data:any){
    this.contactId = data.id;
  }

  campaignHtml:any = null;
  getCampaignPerview($id:any){
    console.log($id);
    if($id > 0 && this.contactId > 0){
      let data  = {
        "campaign_id": $id,
        "lead_id": this.route.snapshot.params['id'],
        "lead_contact_id": this.contactId,
      };
      this.leadApi.getCampaignPerview(data).subscribe({
        next:(value:any) => {
          if(value.success){
            this.campaignHtml = value.html;
          }
        },
      });
    }
  }

  sendCampaign(data:any){
    console.log('76',data);
    //let pipe = new DatePipe('en-US');
    //console.log(pipe.transform(data.date, 'MM/dd/yyyy'))
    if(this.sendCampaignForm.invalid){
      this.sendCampaignFormErr = true;
      Swal.fire({
        heightAuto: false,
        title: '',
        text: "Please fill all the required fields.",
        icon: 'warning',
      });
    }else{
      this.sendCampaignFormErr = false;
      let dataSend:any = {
        "list_selection_type": "all_selected",
        "campaign_id": data.campaign_id,
        "id": [
          this.route.snapshot.params['id']
        ],
        "email_list": [
          data.contact.email
        ],
        "contact_id": [
          data.contact.id
        ],
      }
  
      if(data.bcc){
        var bcc = data.bcc.split(","); 
        dataSend.lead_campaign_bcc_emails = JSON.stringify(bcc);
      }
  
      if(data.date){
        let date = data.date;
        let time = (data.time)?data.time+':00':'00:00:00';
        dataSend.schedule_time = date+' '+time;
      }
      console.log(dataSend);
      
      this.leadApi.getCampaignSend(dataSend).subscribe({
        next:(res:any) => {
            if(res.success){
              this.campaignHtml = null;
              this.sendCampaignForm.reset();
              Swal.fire({
                heightAuto: false,
                title: '',
                text: res.message,
                icon: 'success',
              });
            }
        },
        error:(err:any) => {
            console.log(err.message);
            Swal.fire({
              heightAuto: false,
              title: '',
              text: err.message,
              icon: 'warning',
            });
        },
      });
    }
  }

}
