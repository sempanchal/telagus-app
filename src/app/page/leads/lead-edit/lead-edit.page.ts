import { Component, OnInit, ViewChild } from '@angular/core';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import { LeadsService } from 'src/app/service/leads.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forEach } from 'lodash';
import Swal from 'sweetalert2';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'app-lead-edit',
  templateUrl: './lead-edit.page.html',
  styleUrls: ['./lead-edit.page.scss'],
})
export class LeadEditPage implements OnInit {
  @ViewChild('ajaxloder') ajaxloder: HeaderComponent | any;
  ajaxCall:boolean = false
  activeTab:any = 'lead_activities'; 

  assignedToteam: any = [];
  leadEditFormSubmit:boolean = false;
  leadEditForm!: FormGroup;
  constructor(
    public tokenStorage:TokenStorageService,
    private leadApi: LeadsService,
    private fb: FormBuilder,
    public route: ActivatedRoute,
    public helper: HelperService
  ) { 
    var leadID = this.route.snapshot.params['id']
    this.leadEditForm = this.fb.group({
      lead_id: parseInt(leadID),
      leadclosedate: '',
      price: ['', [Validators.required, Validators.pattern("^[0-9]+([.][0-9]+)?$")]],
      form_page: [''],
      ip: [''],
      assigned_to: [''],
      lead_source: [''],
      message: [''],
      lead_title: [''],
    });
  }

  ngOnInit() {
    this.assignedToteam = this.tokenStorage.getUser().user.client.users;
    this.getLeadsCustomFieldsData();
    this.getLeadsData();
  }

  setActive(tab:any){
    this.activeTab = tab;
  }

  dynamicFieldPosition:any = null;
  //dynamicFieldPositionStatus:boolean = true;
  fieldLength:number = 0;
  getFieldCustomPosition(){
    let user_id = this.tokenStorage.getUser().user.id;
    if(user_id){
      this.dynamicFieldPosition = null;
      this.leadApi.getFieldGridOfLeads(user_id).subscribe({
        next:(res:any) =>{
          if(res['status']) {
            this.dynamicFieldPosition = JSON.parse(res['field_grid_of_leads']);
            if(this.dynamicFieldPosition.length > 0){
              this.dynamicFieldPosition.forEach((element:any) => {

                if(element.id){
                  let value = (this.leadData.custom_fields[element.id]?.value)?this.leadData.custom_fields[element.id]?.value:null;
                  if (element.field_type == 'checkboxes') {
                    let group: any = this.fb.group({});
                    element.options.forEach((option:any) => {
                      if(value){
                        let setval = (value.split(",").includes(option.value))?true:false;
                        group.addControl(option.value, this.fb.control(setval));
                      }else{
                        group.addControl(option.value, this.fb.control(null));
                      }
                    });
                    this.leadEditForm.addControl('lead_custom_' + element.id, group);
                  }else{
                    if(element.required){
                      this.leadEditForm.addControl('lead_custom_' + element.id, this.fb.control(value, Validators.required));
                    }else{
                      this.leadEditForm.addControl('lead_custom_' + element.id, this.fb.control(value));
                    }
                  }
                }
                this.fieldLength+=1;
              });
            }
          }else{
            if(this.leadCustomFields){
              if(this.leadCustomFields.leads.length > 0){
                this.dynamicFieldPosition = this.combineCustomAndSystemFields(this.leadCustomFields.leads);
                this.dynamicFieldPosition.forEach((element:any) => {

                  if(element.id){
                    let value = (this.leadData.custom_fields[element.id]?.value)?this.leadData.custom_fields[element.id]?.value:null;
                    if (element.field_type == 'checkboxes') {
                      let group: any = this.fb.group({});
                      element.options.forEach((option:any) => {
                        if(value){
                          let setval = (value.split(",").includes(option.value))?true:false;
                          group.addControl(option.value, this.fb.control(setval));
                        }else{
                          group.addControl(option.value, this.fb.control(null));
                        }
                      });
                      this.leadEditForm.addControl('lead_custom_' + element.id, group);
                    }else{
                      if(element.required){
                        this.leadEditForm.addControl('lead_custom_' + element.id, this.fb.control(value, Validators.required));
                      }else{
                        this.leadEditForm.addControl('lead_custom_' + element.id, this.fb.control(value));
                      }
                    }
                  }
                  this.fieldLength+=1;
                });
              }
            }
            
          }
        },
        complete: () => {
        }
      });
    }
  }

  combineCustomAndSystemFields(customFields: any = []) {
    
    customFields.forEach((element:any) => {
      element.formControlName = "lead_custom_"+element.id;
      //customFields
    });
    let systemFields = [
      {formControlName: 'assigned_to'},
      {formControlName: 'leadclosedate'},
      {formControlName: 'lead_title'},
      {formControlName: 'price'},
      {formControlName: 'other_name'},
      {formControlName: 'other_date'},
      {formControlName: 'other_deal_position_dropdown'},
      {formControlName: 'form_page'},
      {formControlName: 'ip'},
      {formControlName: 'lead_source'},
      {formControlName: 'message'},
    ];
    let fields: any = [] = [...systemFields, ...customFields];
    return fields;
  }

  onCheckboxChange(event: any, key: any, Id: any) {
    
  }

  leademail:any = null;
  updateALeadData(data:any){
    if(this.leadEditForm.invalid){
      this.leadEditFormSubmit = true;
    }else{
      this.leadEditFormSubmit = false;
      this.leadData.lead_contacts.forEach((element:any) => {
        if (element.is_primary_contact == 1) {
          this.leademail = element.email;
        }
      });

      let param = {
        email: this.leademail,
        lead_id: this.route.snapshot.params['id']
      }
      this.leadApi.checkExistingEmail(param).subscribe({ 
        next:(res:any)=>{
          if(res.is_email){
            Swal.fire({
              heightAuto: false,
              title: 'Opps!',
              text: "This Email is already existing.",
              showDenyButton: true,
              showCancelButton: true,
              confirmButtonText: 'Yes, Save it.',
              denyButtonText: 'Remove All Old Duplicates?',
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#23272b',
              customClass: {
                actions: 'my-actions',
                cancelButton: 'order-3 right-gap',
                confirmButton: 'order-2',
                denyButton: 'order-1',
              }
            }).then((result) => {
              if (result.isConfirmed) {
                this.updateLead(data);
              } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire({
                  heightAuto: false,
                  title: 'Are you sure?',
                  text: "You won't be able to revert this!",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes, Remove it!'

                }).then((result) => {
                  let param = {
                    email: this.leademail,
                    lead_id: data.lead_id
                  }
                  this.leadApi.removeDuplicate(param).subscribe({
                    next:(res:any)=>{
                      Swal.fire({
                        heightAuto: false,
                        title: 'Updated',
                        text: "Duplicate record has been deleted sucessfully.",
                        icon: 'success',
                      });
                    }
                  });
                });
              }
            })
          }else{
            this.updateLead(data);
          }
        }
      });
    }
  }

  updateLead(leaddata:any){
    let primary_contact_first_name;
    let primary_contact_last_name;

    this.leadData.lead_contacts.forEach((element: any) => {
      if (element.is_primary_contact === 1) {
        primary_contact_first_name = element.first_name;
        primary_contact_last_name = element.last_name;
      }
    });

    const formData: FormData = new FormData();
    formData.append('close_date', (leaddata.leadclosedate) ? leaddata.leadclosedate : '');
    formData.append('first_name', (primary_contact_first_name) ? primary_contact_first_name : '');
    formData.append('last_name', (primary_contact_last_name) ? primary_contact_last_name : '');
    formData.append('price', (leaddata.price) ? leaddata.price : '');
    formData.append('form_page', (leaddata.form_page) ? leaddata.form_page : '');
    formData.append('ip', (leaddata.ip) ? leaddata.ip : '');
    formData.append('lead_source', (leaddata.lead_source) ? leaddata.lead_source : '');
    formData.append('message', (leaddata.message) ? leaddata.message : '');
    formData.append('assigned_to', (leaddata.assigned_to) ? leaddata.assigned_to : '');
    formData.append('lead_title', (leaddata.lead_title) ? leaddata.lead_title : '');

    if (this.leadCustomFields.leads) {
      let field_name: any;
      let formdataIdFieldName: any;
      let formdataValueFieldName: any;
      let formdataInputFieldName: any;
      let inc = 0;
      this.leadCustomFields.leads.forEach((element:any) => {
        field_name = "lead_custom_" + element.id;
        formdataIdFieldName = "custom_fields[" + inc + "][id]";
        formdataValueFieldName = "custom_fields[" + inc + "][value]";
        formdataInputFieldName = "custom_fields[" + inc + "][field_type]";
        if (element.field_type == 'checkboxes') {
          let ids: string[] = [];
          for (var key in this.leadEditForm.controls) {
            if (field_name == key && this.leadEditForm.controls[key].value) {
              for (var checkVal in this.leadEditForm.controls[key].value) {
                if (this.leadEditForm.controls[key].value[checkVal]) {
                  ids.push(checkVal);
                }
              }

            }
          }
          if (ids.length) {
            formData.append(formdataIdFieldName, element.id);
            formData.append(formdataValueFieldName, ids.join());
            formData.append(formdataInputFieldName, element.field_type);
          }

        } else if (element.field_type == 'file') {
          /*if (this.leadCustomFieldFileToUpload[field_name]) {
            formData.append(formdataIdFieldName, element.id);
            formData.append(formdataValueFieldName, this.leadCustomFieldFileToUpload[field_name]);
            formData.append(formdataInputFieldName, element.field_type);
          }*/

        } else {
          formData.append(formdataIdFieldName, element.id);
          formData.append(formdataValueFieldName, (this.leadEditForm.controls[field_name].value) ? this.leadEditForm.controls[field_name].value : '');
          formData.append(formdataInputFieldName, element.field_type);
        }
        inc++;

      });
    }

    this.leadApi.updateLeadData(formData,leaddata.lead_id).subscribe({
      next:(res:any) => {
        this.ajaxloder.ajaxCallFunction(true);
        if (res) {
          Swal.fire({
            heightAuto: false,
            title: 'Updated',
            text: "Lead has been updated Successfully.",
            icon: 'success',
          });
        }
      },
      error:(err) =>{
        setTimeout(() => {
          this.ajaxloder.ajaxCallFunction(false);
        }, 1000);
        Swal.fire({
          heightAuto: false,
          title: 'Error',
          text: "Something went wrong",
          icon: 'error',
        });
      },
      complete:()=> {
        setTimeout(() => {
          this.ajaxloder.ajaxCallFunction(false);
        }, 1000);
      },
    })
  }

  leadData:any = null;
  ajaxLoader:boolean = true;
  getLeadsData(){
    this.ajaxLoader = true;
    var leadID = this.route.snapshot.params['id']
    this.leadApi.getLeadsDataFun(leadID).subscribe({
      next:(res:any)=>{
        let leadclosedate = (res.close_date)?this.formatDate(res.close_date):null;
        this.leadData = res;
        this.leadEditForm.patchValue({
          lead_id: res.id,
          leadclosedate: leadclosedate,
          price: res.price,
          form_page: res.form_page,
          ip: res.ip,
          assigned_to: Number(res.assigned_to),
          lead_source: res.lead_source,
          message: res.message,
          lead_title: res.lead_title,
        });
      },
      error:(err) =>{
        this.ajaxLoader = false;
      },
      complete: () => {
        //this.getLeadsCustomFieldsData();
        this.ajaxLoader = false;
        this.getFieldCustomPosition();
        
      },
    })
  }

  formatDate(date:any) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }

  leadCustomFields:any = null;
  getLeadsCustomFieldsData(){
    var leadID = this.route.snapshot.params['id']
    this.leadApi.getLeadsCustomFieldsData(leadID).subscribe({
      next:(res:any)=>{
        this.leadCustomFields = res.custom_fields;
      },
      complete: () => {
        //this.getFieldCustomPosition();
      },
    })
  }
}
