import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderModule } from 'src/app/components/header/header.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { IonicModule } from '@ionic/angular';
import { QuotesPageRoutingModule } from './quotes-routing.module';
import { QuotesPage } from './quotes.page';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ViewQuoteComponent } from './view-quote/view-quote.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HeaderModule,
    QuotesPageRoutingModule,
    NgxPaginationModule,
    IonicModule.forRoot(),
  ],
  declarations: [QuotesPage,ViewQuoteComponent]
})
export class QuotesPageModule {}
