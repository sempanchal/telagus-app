import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuotesPage } from './quotes.page';
import { ViewQuoteComponent } from './view-quote/view-quote.component'; 

const routes: Routes = [
  {
    path: '',
    component: QuotesPage
  },
  {
    path: 'view/:id',
    component: ViewQuoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuotesPageRoutingModule {}
