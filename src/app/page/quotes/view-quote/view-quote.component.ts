import { Component, OnInit, ViewChild } from '@angular/core';
import { QuotesService } from 'src/app/service/quotes.service';
import { HelperService } from 'src/app/service/helper.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Filesystem,Directory } from '@capacitor/filesystem';
import { Browser } from '@capacitor/browser';
import { IonModal } from '@ionic/angular';
import { TokenStorageService } from 'src/app/service/token-storage.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { FilePicker } from '@capawesome/capacitor-file-picker';

@Component({
  selector: 'app-view-quote',
  templateUrl: './view-quote.component.html',
  styleUrls: ['./view-quote.component.scss'],
})
export class ViewQuoteComponent  implements OnInit {
  @ViewChild(IonModal) custModal!: IonModal;
  ajaxCall:boolean = true;
  quote:any = null;
  quoteForm:any = null
  estimateItems:any = null;

  sub_total:any = null;
  discount:any = null;
  sub_total_after_discount:any = null;
  vat:any = null;
  vatValue:any = null;
  total:any = null;
  quote_attachment!: FormGroup;
  attachmentBtn:boolean = false;
  quoteAttachment!: FormGroup;
  attachmentSubmit:boolean = false;
  attachment:any = null;
  sendQuoteMailForm!: FormGroup;
  formSubmit:boolean = false;
  sendAjaxCall:boolean = false;
  teamMember:any = [];
  logoUrl:any = null;

  discountForm!: FormGroup;
  discountFormBtn:boolean = false;

  constructor(
    private quoteApi:QuotesService,
    public helper:HelperService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public tokenStorage:TokenStorageService,
  ) { 
    this.sendQuoteMailForm = this.fb.group({
      to: ['', Validators.required],
      cc: [''],
      bcc: [''],
      reply_to: [''],
      default_reply_to: ['', Validators.required],
      msg: [''],
    });

    this.quoteAttachment = this.fb.group({
      file: ['', Validators.required],
      title: ['', Validators.required],
    });

    this.quote_attachment = this.fb.group({
      estimate_attachment_id: ['', Validators.required],
    });

    this.discountForm = this.fb.group({
      discount_type: ['', [Validators.required]],
      discount: ['', [Validators.required, Validators.min(0), Validators.pattern("^[0-9]+([.][0-9]+)?$")]],
      quote_id: [this.route.snapshot.params['id'], Validators.required]
    });
  }

  ngOnInit() {
    this.checkPermission();
    this.getQuote();
    this.getEstimateAttachment();
    this.sendQuoteMailForm.patchValue({
      default_reply_to:this.tokenStorage.getUser().user.client.client_settings.email,
    }) 

    if (this.tokenStorage.getUser().user.client.logo) {
      this.logoUrl = environment.AWSBucketURL + 'client-logo/' + this.tokenStorage.getUser().user.client.logo;
    } else {
      this.logoUrl = environment.AWSBucketURL + 'logo_placeholder.png';
    }
    
    this.tokenStorage.getUser().user.client.users.forEach((element: any) => {
      if (element.email) {
        this.teamMember.push({
          id: element.email,
          text: element.first_name + ' ' + element.last_name + ' | ' + element.email
        });
      }
    });
  }

  getQuote(){
    var id = this.route.snapshot.params['id'];
    this.quoteApi.quoteWithItems(id).subscribe({
      next:(res:any) => {
          console.log(res);
          this.quote = res.quote.estimate;
          this.sendQuoteMailForm.patchValue({
            to:res.quote.estimate.lead_email,
          });
          this.quoteForm = res.quote.estimate_creator_data;
          this.estimateItems = res.quote.estimate_items_data;

          this.sub_total = res.quote.estimate_items_total;
          this.discount = res.quote.estimate.discount;
          this.sub_total_after_discount = res.quote.estimate.total_amount_after_discount;
          this.vat = res.quote.tax_id;
          this.vatValue = res.quote.estimate_tax_id;
          this.total = res.quote.estimate_items_all_total;

          console.log(this.quoteForm);
      },complete:()=> {
          this.ajaxCall = false;
          this.getAttachmentForIndQuote();
      },
    })
  }
  quoteAttachments:any = null;
  quoteAttachmentsPath = environment.AWSBucketURL+'estimate-attachment/';
  getAttachmentForIndQuote(){
    var id = this.route.snapshot.params['id'];
    this.quoteApi.getAttachmentForIndQuote(id).subscribe({
      next:(res:any) => {
          console.log("124",res.estimate_attachment_list);
          this.quoteAttachments = res.estimate_attachment_list;
      },
    })
  }

  removeAttachmentfromQuote(id: any) {
    this.quoteApi.deleteAttachmentForIndQuote(id).subscribe((res: any) => {
      Swal.fire({
        heightAuto: false,
        title: 'Success',
        text: res.message,
        icon: 'success',
      }).then((result) => {
        this.getAttachmentForIndQuote();
      });
    })
  }

  downloadpdf() {
    this.quoteApi.downloadPDF(this.route.snapshot.params['id']).subscribe({
      next:(res:any) => {
        this.saveFile(res);
      },
      error:(err)=> {
        console.log(err);
      },
    });
  }

  saveFile(res:any){
    const blob = new Blob([res], {type: "application/octet-stream"});
    var reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = () => {
      let base64 = reader.result?.toString();
      if(base64){
        Filesystem.writeFile({
          path: 'quote-'+Math.ceil(Math.random() * 100)+'-'+this.route.snapshot.params['id']+'.pdf',
          data: base64,
          directory:Directory.Documents,
        }).then((res:any)=>{
          this.helper.openFileWithType(res.uri);
          let title = 'Quote #'+this.route.snapshot.params['id'];
          this.helper.showLocalNotification(title,'Download quote successfully.');
        },(err)=>{
          alert(err);
        })
      }
    }
  }

  async checkPermission() {
    let permission = await Filesystem.checkPermissions()
    if (permission.publicStorage !== "granted") {
      permission = await Filesystem.requestPermissions() // Why this doesn't work????
      if (permission.publicStorage !== "granted") {
        return
      }
    }else{
      console.log(107,permission);
    }
  }
  
  sendMailPopup(isOpen: boolean){
    this.custModal.isOpen = isOpen;
  }


  cancel(isOpen: boolean) {
    this.custModal.isOpen = isOpen;
  }

  sendQuote(data:any){
    if (this.sendQuoteMailForm.invalid) {
      this.formSubmit = true;
    } else {
      this.sendAjaxCall = true;
      this.quoteApi.sendQuoteEmail(data, this.route.snapshot.params['id']).subscribe({
          next:(res: any) => {
            Swal.fire({
              heightAuto: false,
              title: 'Success',
              text: res.message,
              icon: 'success',
            }).then(() => {
              this.getQuote();
              this.custModal.isOpen = false;
            });
          },
          complete:()=> {
            this.sendAjaxCall = false;
          },
        })
    }
  }

  attachmentListModal:boolean = false;
  attachmentListModalPopup(isOpen: boolean){
    this.attachmentListModal = isOpen;
  }

  attachmentsModal:boolean = false;
  attachPopup(isOpen: boolean){
    this.attachmentsModal = isOpen;
  }



  attachmentList:any = null;
  getEstimateAttachment(){
    this.quoteApi.estimateAttachment().subscribe({
      next:(res:any) => {
        this.attachmentList = res.estimate_attachment;
        console.log(this.attachmentList);
      },
    })
  }

  selectedFileName:any = null;
  async pickFiles(){
    const result = await FilePicker.pickFiles({
      types: ['image/*',
        'application/*',
      ],
      limit:1,
    });
    if(result){
      this.selectedFileName = result.files[0].name;
      alert(this.selectedFileName);
      const file = result.files[0];
      this.quoteAttachment.patchValue({
        file: file.blob,
      });
    }
  };

  removeSelectedFile(){
    this.selectedFileName = null;
    this.quoteAttachment.reset();
  }

  quoteAttachmentSubmit(data:any){
    if(this.quoteAttachment.invalid){
      this.attachmentSubmit = true;
    }else{
      this.attachmentSubmit = false;
      this.quoteApi.addFilestoinQuotes(data).subscribe({
        next:(res:any) => {
          Swal.fire({
            heightAuto: false,
            title: 'Success',
            text: res.message,
            icon: 'success',
          }).then(() => {
            this.getEstimateAttachment();
            this.quoteAttachment.reset();
            this.attachmentsModal = false;
          });
        },
      })
    }
  }

  selectAttachmentItem(data:any){
    if(this.quote_attachment.invalid){
      this.attachmentBtn = true;
    }else{
      this.attachmentBtn = false;
      data.estimate_id = this.route.snapshot.params['id']
      this.quoteApi.setAttachmentsToQuote(data).subscribe({
        next:(res:any) => {
            console.log(res);
            Swal.fire({
              heightAuto: false,
              title: 'Success',
              text: res.message,
              icon: 'success',
            }).then(()=>{
              this.quote_attachment.reset();
              this.attachmentListModal = false;
            });
        },
      });
    }
  }

  discountTypes: any = [
    {id: 'Fixed', text: 'Fixed'},
    {id: 'Percentage', text: 'Percentage'}
  ]
  discountModal:boolean = false;
  popup_title:any = 'Add';
  discountModalPopup(isOpen: boolean){
    this.discountModal = isOpen;
  }

  addDiscount(data:any){
    if(this.discountForm.invalid){
      this.discountFormBtn = true;
    }else{
      this.discountFormBtn = false;
      this.quoteApi.addDiscount(data).subscribe({
        next:(res: any) => {
          
          Swal.fire({
            heightAuto: false,
            title: 'Success',
            text: res.message,
            icon: 'success',
          }).then(()=>{
            this.discountForm.reset();
            this.discountForm.patchValue({
              'quote_id': this.route.snapshot.params['id']
            });
            this.getQuote();
            this.discountModal = false;
          });
        },
      });
    }
  }

  removeDiscount(quote_id:any) {
    Swal.fire({
      heightAuto: false,
      title: 'Are you sure?',
      text: "Do you want to remove discount?",
      showCancelButton: true,
      icon: 'warning',
      cancelButtonColor: '#d33',
      confirmButtonColor: '#3085d6',
      confirmButtonText: 'Yes, remove!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.quoteApi.removeDiscount(quote_id).subscribe((res: any) => {
            if(res.success) {
              Swal.fire({   
                heightAuto: false,
                title: 'Success',
                text: res.message,
                icon: 'success',
              }).then(() => {
                this.getQuote();
              })
            }else{
              Swal.fire({
                heightAuto: false,
                title: 'Error',
                text: res.message,
                icon: 'error',
               }).then(() => {
                this.getQuote();
              })
            }
          },
          error => {
            console.log(error);
          })
      }
    })
  }

  updateDiscount(content:any) {
    console.log(content);
    this.discountForm.patchValue({
      'quote_id': this.route.snapshot.params['id'],
      'discount_type': content.discount_type,
      'discount': content.discount_val,
    });
    this.popup_title = 'Edit';
    this.discountModal = true;
  }
}
