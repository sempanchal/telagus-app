import { Component, OnInit } from '@angular/core';
import { QuotesService } from 'src/app/service/quotes.service';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.page.html',
  styleUrls: ['./quotes.page.scss'],
})
export class QuotesPage implements OnInit {
  ajaxCall:boolean = true;
  public count = 0;
  public itemsPerPage:number = 10;
  public currentPage = 1;
  estimate:any = [];
  public ajaxLoader:boolean = true;

  constructor(
    private quoteApi:QuotesService,
    public helper:HelperService
  ) { }

  ngOnInit() {
    this.getQuotes({'page':this.currentPage,'limit':this.itemsPerPage});
  }

  getQuotes(data:any){
    this.estimate = [];
    this.ajaxLoader = true;
    this.quoteApi.quoteListing(data).subscribe({
      next:(res:any) => {
        this.estimate = res.estimate;
        this.count =  res.total;
      },
      complete:()=> {
        this.ajaxCall = false;
        this.ajaxLoader = false;
      },
    })
  }

  public onChange(event:any): void {
    this.currentPage = event;
    this.getQuotes({'page':this.currentPage,'limit':this.itemsPerPage});
  }

  goToPage($event:any){
      this.getQuotes({'page':$event.target.value,'limit':this.itemsPerPage});
  }
  
}
