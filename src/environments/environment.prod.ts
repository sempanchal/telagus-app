export const environment = {
  production: true,
  apiURL: 'https://api.telagus.com/api/',
  AWSBucketURL:'https://telagus-crm-bucket.s3.eu-west-2.amazonaws.com/app/public/',
};
