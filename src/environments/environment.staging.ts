export const environment = {
  production: false,
  apiURL: 'https://api.telsacrm.co.uk/api/',
  AWSBucketURL:'https://telagus-crm-bucket.s3.eu-west-2.amazonaws.com/app/public/',
};
